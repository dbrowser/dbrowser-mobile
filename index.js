/* Gateway Browser
 * Copyright (C) 2020 Gateway Browser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/**
 * @format
 */
import 'react-native-gesture-handler';
import {enableScreens} from 'react-native-screens';
enableScreens();

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

global.protocolInfo = {};

AppRegistry.registerComponent(appName, () => App);
