/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import {refreshSvg, bigXSvg} from '../svg';
import {ActionCreators} from '../actions';

const ReloadButton = ({
  loading,
  tabIndex,
  tabHistory,
  pushHistory,
  reloadWebView,
  stopLoadingWebView,
}: props) => {
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const currentPage = history.get(currentIndex);
  const isHttp =
    currentPage.uri && currentPage.uri.indexOf('http') === 0 ? true : false;
  const progress = loading.get(tabIndex);
  const isLoading = progress < 1;
  const reload = () => {
    if (isLoading) {
      // TODO M - Stopping non-http reloading
      stopLoadingWebView(tabIndex);
    } else {
      isHttp
        ? reloadWebView(tabIndex)
        : pushHistory(tabIndex, currentPage, true);
    }
  };
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={reload}
      onLongPress={reload}>
      {isLoading ? (
        <ThemeSvg width="20" height="20" svgXmlData={bigXSvg} />
      ) : (
        <ThemeSvg
          width="20"
          height="17"
          svgXmlData={refreshSvg}
          style={styles.svg}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  svg: {
    marginLeft: 6,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabHistory: state.tabHistory,
    loading: state.loading,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReloadButton);
