/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {Animated, Easing} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import NavigationService from '../lib/NavigationService';
import SlideUpModalBackground from './SlideUpModalBackground';
import BottomCard from './BottomCard';
import {ActionCreators} from '../actions';

const BottomCardModal = ({
  dashboard,
  children,
  deviceDimensions,
  style,
}: props) => {
  const height = deviceDimensions.get('height');
  const [y] = useState(new Animated.Value(height));

  useEffect(() => {
    Animated.timing(y, {
      toValue: 0,
      duration: 350,
      easing: Easing.inOut(Easing.quad),
      useNativeDriver: true,
    }).start();
  }, []);

  const animatedExit = () => {
    Animated.timing(y, {
      toValue: height,
      duration: 350,
      easing: Easing.inOut(Easing.quad),
      useNativeDriver: true,
    }).start(() => {
      NavigationService.goBack();
    });
  };
  return (
    <SlideUpModalBackground
      dashboard={dashboard}
      animatedY={y}
      onPress={animatedExit}>
      <BottomCard animatedY={y} style={style}>
        {children}
      </BottomCard>
    </SlideUpModalBackground>
  );
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BottomCardModal);
