/**
 * @format
 * @flow
 */

import React from 'react';
import {Platform} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const {List} = require('immutable');

import PopupModalWindow from '../views/PopupModalWindow';
import PopupModalList from '../views/PopupModalList';
import PopupModalListItem from '../views/PopupModalListItem';
import NavigationService from '../lib/NavigationService';
import {getProtocolSchema, parseSchemaIcon} from '../lib/common';
import {ActionCreators} from '../actions';
import {navigationArrowSvg, refreshSvg, infoSvg, shareSvg} from '../svg';

const OptionsPopup = ({
  tabIndex,
  protocols,
  tabHistory,
  navForward,
  pushHistory,
  reloadWebView,
}: props) => {
  const screenRatio = 4 / 5;
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const currentPage = history.get(currentIndex);
  const url = currentPage.inputUrl || currentPage.uri;
  let listData = List([
    {
      icon: infoSvg,
      title: 'Protocol Info',
      iconType: 'textColor',
      onPress: async () => {
        NavigationService.replace({
          name: 'ProtocolInfo',
          params: {
            tabIndex,
          },
        });
      },
    },
    {
      icon: shareSvg,
      title: 'Share Page',
      iconType: 'textColor',
      onPress: () => {
        NavigationService.replace({
          name: 'Share',
          params: {
            tabIndex,
          },
        });
      },
    },
  ]);
  if (Platform.OS === 'android') {
    let actions = [
      {
        icon: refreshSvg,
        title: 'Reload',
        iconType: 'textColor',
        onPress: async () => {
          url.search(/https?:\/\//) === 0
            ? reloadWebView(tabIndex)
            : pushHistory(tabIndex, currentPage, true);
          NavigationService.goBack();
        },
      },
      {title: 'seperator', seperator: true},
    ];
    if (currentIndex > 0) {
      actions.unshift({
        icon: navigationArrowSvg,
        iconStyle: {transform: [{scaleX: -1}]},
        title: 'Forward',
        iconType: 'textColor',
        onPress: async () => {
          navForward(tabIndex, 1);
          NavigationService.goBack();
        },
        onLongPress: async () => {
          NavigationService.replace({
            name: 'TabHistoryPopup',
            params: {
              tabIndex,
              isBack: false,
            },
          });
        },
      });
    }
    const list = List(actions);
    listData = list.concat(listData);
  }
  if (url.search(/file:\/\/|https?:\/\//i) !== 0) {
    const currentContext = currentPage.context || 'site';
    let list = List([]);
    const schema = getProtocolSchema(url, protocols);
    if (schema.browserContexts) {
      const filteredContexts = schema.browserContexts
        .filter(ctx => ctx.name !== currentContext)
        .map(ctx => {
          return {
            icon: parseSchemaIcon(ctx.icon).asset,
            title: `Open as ${ctx.name}`,
            iconType: 'textColor',
            onPress: () => {
              pushHistory(tabIndex, {inputUrl: url, request: ctx.name});
              NavigationService.goBack();
            },
          };
        });
      list = List(filteredContexts);
    }
    if (list.size > 0) {
      // Add seperator
      list = list.unshift({title: 'seperator', seperator: true});
    }
    listData = listData.concat(list);
  }
  const renderItem = ({item, index}) => {
    const maxIndex = listData.size - 1;
    return (
      <PopupModalListItem
        index={index}
        maxIndex={maxIndex}
        numberOfLines={1}
        {...item}
      />
    );
  };

  const keyExtractor = (item, index) => {
    return item.title + index;
  };

  return (
    <PopupModalWindow screenRatio={screenRatio}>
      <PopupModalList
        screenRatio={screenRatio}
        immutableData={listData}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </PopupModalWindow>
  );
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    protocols: state.protocols,
    tabHistory: state.tabHistory,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OptionsPopup);
