/**
 * @format
 * @flow
 */

import React from 'react';
import {Animated} from 'react-native';
import {connect} from 'react-redux';

const AnimatedThemeView = ({
  type,
  theme,
  children,
  style,
  opacity,
  ...props
}: props) => {
  const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
  const themeType = type || 'backgroundColor';
  const color = theme.get(subState).get(themeType);
  const backgroundColor = opacity
    ? color.replace('1.0', opacity.toString())
    : color;
  return (
    <Animated.View {...props} style={[style, {backgroundColor}]}>
      {children}
    </Animated.View>
  );
};

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(AnimatedThemeView);
