/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import {navigationArrowSvg} from '../svg';
import {ActionCreators} from '../actions';
import NavigationService from '../lib/NavigationService';

class NavigationButton extends Component {
  constructor() {
    super();
    this.button = null;
  }
  onPress = () => {
    const {
      props: {tabIndex, isBack, navBack, navForward},
    } = this;
    isBack ? navBack(tabIndex, 1) : navForward(tabIndex, 1);
  };
  onLongPress = () => {
    const {
      props: {tabIndex, isBack},
    } = this;
    NavigationService.navigate({
      name: 'TabHistoryPopup',
      params: {
        tabIndex,
        isBack,
      },
    });
  };
  onLayout = () => {
    const {
      props: {setNavButtonPosition, isBack},
    } = this;
    this.button.measure((x, y, width, height, pageX, pageY) => {
      setNavButtonPosition(isBack, pageX, pageY);
    });
  };
  render() {
    const {
      onPress,
      onLongPress,
      onLayout,
      props: {tabIndex, isBack, tabHistory},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    var disabled = isBack
      ? currentIndex >= history.size - 1
      : currentIndex <= 0;
    return (
      <TouchableOpacity
        ref={ref => (this.button = ref)}
        style={[styles.touchable, {transform: [{scaleX: isBack ? 1 : -1}]}]}
        onPress={onPress}
        onLongPress={onLongPress}
        onLayout={onLayout}
        disabled={disabled}>
        <ThemeSvg
          width="12"
          height="20"
          svgXmlData={navigationArrowSvg}
          opacity={disabled ? 0.4 : 1}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabHistory: state.tabHistory,
    // canGoBack: state.canGoBack,
    // canGoForward: state.canGoForward,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationButton);
