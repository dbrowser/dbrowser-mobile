/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useFocusEffect} from '@react-navigation/native';

import {ActionCreators} from '../actions';

import NavigationService from '../lib/NavigationService';

const AndroidBackHandler = ({
  tabIndex,
  tabHistory,
  tabManagement,
  navBack,
}: props) => {
  const {currentIndex, history} = tabHistory.get(tabIndex);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (tabIndex === tabManagement.get('currentTab')) {
          if (currentIndex < history.size - 1) {
            NavigationService.navigate({
              name: 'TabHistoryPopup',
              params: {
                tabIndex,
                isBack: true,
              },
            });
          } else {
            BackHandler.exitApp();
          }
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [currentIndex, history.size, tabIndex, tabManagement]),
  );

  return null;
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabManagement: state.tabManagement,
    tabHistory: state.tabHistory,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AndroidBackHandler);
