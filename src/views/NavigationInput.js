/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  Image,
  TextInput,
  StyleSheet,
  Platform,
  UIManager,
  TouchableOpacity,
  // LayoutAnimation,
} from 'react-native';
import {connect} from 'react-redux';
import Share from 'react-native-share';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';
import ActionSheet from 'react-native-action-sheet';

import ThemeSvg from './ThemeSvg';
import ThemeView from './ThemeView';
import NavigationHostButton from './NavigationHostButton';
import NavigationService from '../lib/NavigationService';
import {shareSvg, smallCircleXSvg, scaledWedgeSvg} from '../svg';
import {ActionCreators} from '../actions';
const basicParseURL = require('url-parse');

// if (
//   Platform.OS === 'android' &&
//   UIManager.setLayoutAnimationEnabledExperimental
// ) {
//   UIManager.setLayoutAnimationEnabledExperimental(true);
// }
class NavigationInput extends Component {
  constructor() {
    super();
    this.input = null;
    this.submitting = false;
    this.state = {
      inputText: undefined,
      containerWidth: 9999,
      containerHeight: 36,
      inputContentWidth: 0,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const currentUrl = this.getUrl(this.props);
    const nextUrl = this.getUrl(nextProps);
    if (currentUrl !== nextUrl) {
      this.setState({inputText: nextUrl});
    }
    return true;
  }

  onChangeText = text => {
    this.setState({inputText: text});
  };

  clearText = () => {
    this.setState({inputText: ''});
  };

  share = async () => {
    const {
      props: {tabIndex},
    } = this;
    NavigationService.navigate({
      name: 'Share',
      params: {
        tabIndex,
      },
    });
  };

  containerLayout = ({
    nativeEvent: {
      layout: {width, height},
    },
  }) => {
    this.setState({containerWidth: width, containerHeight: height});
  };

  inputLayout = ({
    nativeEvent: {
      contentSize: {width},
    },
  }) => {
    this.setState({inputContentWidth: width});
  };

  inputFocused = ({nativeEvent}) => {
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.toggleInputSelected(true);
  };

  wrapperPress = () => {
    this.input.focus();
  };

  wrapperLongPress = () => {
    this.share();
  };

  submit = ({nativeEvent: {text, eventCount, target}}) => {
    const {
      checkSearch,
      encodedString,
      props: {tabIndex, pushHistory, browserInputSettings},
    } = this;
    var trimmedText = text.trim();
    if (trimmedText === '') {
      return;
    }
    this.submitting = true;
    const isSearch = checkSearch(trimmedText);
    if (isSearch) {
      let searchText = trimmedText;
      if (browserInputSettings.get('searchEngine') === 'Google') {
        searchText = searchText.concat(' !g');
      }
      const searchParams = encodedString(searchText);
      const submitText = `https://duckduckgo.com/?q=${searchParams}`;
      pushHistory(tabIndex, {uri: submitText});
    } else {
      if (trimmedText.search(/dat:\/\/|hyper:\/\/|gateway:\/\//i) === 0) {
        // NOTE - If you want to prevent repeat pages when using an equivalent '.html' url, you'd do a check/filter here
        pushHistory(tabIndex, {inputUrl: trimmedText, request: true});
      } else if (trimmedText.indexOf('.') !== -1) {
        if (trimmedText.search(/http/i) === -1) {
          const httpsPrefix = 'https://';
          trimmedText = httpsPrefix.concat(trimmedText);
        }
        if (trimmedText.charAt(trimmedText.length - 1) !== '/') {
          trimmedText = trimmedText.concat('/');
        }
        pushHistory(tabIndex, {uri: trimmedText});
      }
    }
  };

  blur = () => {
    const {getUrl, props} = this;
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (!this.submitting) {
      const fullText = getUrl(props);
      this.setState({inputText: fullText}, () => {
        this.props.toggleInputSelected(false);
      });
    } else {
      this.submitting = false;
      this.props.toggleInputSelected(false);
    }
  };

  getUrl = props => {
    const {tabIndex, tabHistory} = props;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    let currentPage = history.get(currentIndex);
    let input = currentPage.inputUrl || currentPage.uri;
    return input;
  };

  selectedInputText = text => {
    const {getUrl, props} = this;
    if (text === undefined) {
      return getUrl(props);
    }
    return text;
  };

  checkSearch = text => {
    const {
      props: {inputSelected},
    } = this;
    if (
      text !== '' &&
      inputSelected &&
      (text.indexOf(' ') !== -1 ||
        (text.indexOf('/') === -1 && text.indexOf('.') === -1))
    ) {
      return true;
    }
    return false;
  };

  encodedString = text => {
    return encodeURIComponent(text).replace(/%20/g, '+');
  };

  toggleSearchEngine = () => {
    const {
      props: {theme, setBrowserSearchEngine},
    } = this;
    const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
    const highlightColor = theme.get(subState).get('highlightColor');
    const buttonOptions = ['DuckDuckGo', 'Google'];
    if (Platform.OS === 'ios') {
      buttonOptions.push('Cancel');
    }
    const CANCEL_INDEX = 2;

    ActionSheet.showActionSheetWithOptions(
      {
        options: buttonOptions,
        cancelButtonIndex: CANCEL_INDEX,
        tintColor: highlightColor,
        title: 'Default Search Engine',
      },
      buttonIndex => {
        if (
          buttonIndex !== CANCEL_INDEX &&
          buttonIndex !== 'undefined' &&
          buttonIndex !== undefined
        ) {
          const selection = buttonOptions[buttonIndex];
          setBrowserSearchEngine(selection);
        }
      },
    );
  };

  render() {
    const {
      blur,
      share,
      getUrl,
      submit,
      clearText,
      inputLayout,
      checkSearch,
      inputFocused,
      wrapperPress,
      containerLayout,
      wrapperLongPress,
      selectedInputText,
      toggleSearchEngine,
      state: {
        inputText: componentInputText,
        containerWidth,
        containerHeight,
        inputContentWidth,
      },
      props: {style, theme, tabIndex, inputSelected, browserInputSettings},
    } = this;
    const fullUrl = getUrl(this.props);
    const inputText = inputSelected
      ? selectedInputText(componentInputText)
      : basicParseURL(fullUrl).pathname.replace(/index.html|.html/, '');
    const isSearch = checkSearch(inputText.trim());
    const searchEngine = browserInputSettings.get('searchEngine');
    const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
    const highlightColor = theme.get(subState).get('highlightColor');
    const selectionColor =
      Platform.OS === 'ios'
        ? highlightColor
        : highlightColor.replace('1.0', '0.35');
    let textColor = theme.get(subState).get('textColor');
    const placeholderColor = textColor.replace('1.0', '0.5');
    if (!inputSelected) {
      textColor = textColor.replace('1.0', '0.7');
    }
    const inputFlex = inputSelected ? 1 : null;
    const inputLeftMargin = inputSelected ? (isSearch ? 5 : 15) : null;
    const inputWidth = inputSelected
      ? null
      : Math.min(inputContentWidth, containerWidth - 20);
    const roundingStyle = {
      borderRadius: containerHeight / 2,
      borderTopLeftRadius: inputSelected ? containerHeight / 2 : 0,
      borderBottomLeftRadius: inputSelected ? containerHeight / 2 : 0,
    };
    const barMarginLeft = inputSelected ? 0 : 2;
    const {wedgeSvg, height: wedgeHeight, width: wedgeWidth} = scaledWedgeSvg(
      containerHeight,
    );
    return (
      <View style={[styles.container, styles.touchable]}>
        {!inputSelected ? (
          <NavigationHostButton height={containerHeight} tabIndex={tabIndex} />
        ) : null}
        <TouchableOpacity
          style={styles.touchable}
          disabled={inputSelected}
          onPress={wrapperPress}
          onLongPress={wrapperLongPress}
          activeOpacity={0.9}>
          {!inputSelected ? (
            <ThemeSvg
              opacity={0.1}
              width={wedgeWidth}
              height={wedgeHeight}
              svgXmlData={wedgeSvg}
              type="textColor"
              style={[
                styles.wedge,
                {
                  marginLeft: -Number(wedgeWidth),
                },
              ]}
            />
          ) : null}
          <ThemeView
            type="textColor"
            style={[style, roundingStyle]}
            opacity={0.1}
            onLayout={containerLayout}>
            <View style={[styles.bar, {marginLeft: barMarginLeft}]}>
              {inputSelected && isSearch ? (
                <TouchableOpacity
                  style={styles.innerInputButton}
                  onPress={toggleSearchEngine}>
                  <Image
                    style={styles.searchIcon}
                    resizeMode="contain"
                    source={
                      searchEngine === 'Google'
                        ? require('../../assets/img/google-search-icon.png')
                        : require('../../assets/img/ddg-search-icon.png')
                    }
                  />
                </TouchableOpacity>
              ) : null}
              <TextInput
                ref={ref => (this.input = ref)}
                style={[
                  styles.input,
                  {
                    color: textColor,
                    flex: inputFlex,
                    marginLeft: inputLeftMargin,
                    width: inputWidth,
                  },
                ]}
                pointerEvents={inputSelected ? 'auto' : 'none'}
                onContentSizeChange={inputLayout}
                placeholderTextColor={placeholderColor}
                placeholder={inputSelected ? 'Search or type URL' : '/'}
                multiline={false}
                returnKeyType="go"
                selectionColor={selectionColor}
                disableFullscreenUI
                selectTextOnFocus
                blurOnSubmit
                autoCorrect={inputText.indexOf(' ') !== -1}
                underlineColorAndroid="transparent"
                onChangeText={this.onChangeText}
                keyboardType={Platform.OS === 'ios' ? 'web-search' : 'default'}
                onFocus={inputFocused}
                onBlur={blur}
                onSubmitEditing={submit}
                value={inputText}
              />
              {inputSelected ? (
                <TouchableOpacity
                  style={styles.innerInputButton}
                  onPress={clearText}
                  onLongPress={clearText}>
                  <ThemeSvg
                    width="12"
                    height="12"
                    svgXmlData={smallCircleXSvg}
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            {/*!inputSelected ? (
              <TouchableOpacity
                style={[styles.inputButton, styles.rightButton]}
                onPress={share}
                onLongPress={share}>
                <ThemeSvg width="13.9" height="18" svgXmlData={shareSvg} />
              </TouchableOpacity>
            ) : null*/}
          </ThemeView>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  touchable: {
    flex: 1,
    flexBasis: 36,
    flexDirection: 'row',
  },
  input: {
    ...systemWeights.regular,
    marginVertical: Platform.OS === 'ios' ? 10 : 0,
    fontSize: 18,
  },
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  innerInputButton: {
    height: 36,
    width: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputButton: {
    height: 36,
    width: 36,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  searchIcon: {
    width: 21,
    height: 21,
    marginLeft: 5,
    backgroundColor: 'transparent',
  },
  leftButton: {
    left: 0,
  },
  rightButton: {
    right: 0,
  },
  wedge: {
    transform: [
      {
        scaleX: -1,
      },
      {
        scaleY: -1,
      },
    ],
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    browserInputSettings: state.browserInputSettings,
    tabHistory: state.tabHistory,
    theme: state.theme,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationInput);
