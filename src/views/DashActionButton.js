/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {systemWeights} from 'react-native-typography';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import ThemeText from './ThemeText';
import {ActionCreators} from '../actions';

const DashActionButton = ({
  svgXmlData,
  opacity,
  type,
  title,
  onPress,
  deviceDimensions,
}: props) => {
  const orientation = deviceDimensions.get('orientation');
  const buttonWidth =
    orientation === 'landscape'
      ? deviceDimensions.get('height') / 6
      : deviceDimensions.get('width') / 6;
  let orientationStyle = {flexDirection: 'column'};
  let textStyle = {marginTop: 10};
  if (orientation === 'landscape') {
    orientationStyle = {flexDirection: 'row'};
    textStyle = {marginLeft: 10};
  }
  textStyle.opacity = opacity || 1;
  return (
    <TouchableOpacity
      style={[styles.container, orientationStyle]}
      onPress={onPress}>
      <ThemeSvg
        type={type || 'highlightColor'}
        opacity={opacity || 1}
        height={`${buttonWidth}`}
        width={`${buttonWidth}`}
        svgXmlData={svgXmlData}
      />
      <ThemeText
        type={type || 'highlightColor'}
        style={[styles.font, textStyle]}
        allowFontScaling={false}>
        {title}
      </ThemeText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  font: {
    ...systemWeights.regular,
    fontSize: 15,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashActionButton);
