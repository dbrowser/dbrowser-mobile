/**
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';
const basicParseURL = require('url-parse');

import ThemeSvg from './ThemeSvg';
import ThemeText from './ThemeText';
import ThemeView from './ThemeView';
import {ActionCreators} from '../actions';
import {lockSvg, hexagonSvg, cautionSvg} from '../svg';
import {capitalize, getProtocolSchema, parseSchemaIcon} from '../lib/common';
import NavigationService from '../lib/NavigationService';

const ProtocolDescription = ({
  tabIndex,
  pushHistory,
  tabHistory,
  protocols,
}: props) => {
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const source = history.get(currentIndex);
  const url = source.inputUrl || source.uri;
  let title;
  let body;
  let secondaryBody;
  let icon;
  let iconHeight = '16';
  let iconWidth = '16';
  let buttons = [];
  const {protocol, pathname} = basicParseURL(url);
  let currentContext = source.context || source.request;
  if (!currentContext || typeof currentContext !== 'string') {
    currentContext = 'site';
  }
  switch (protocol) {
    case 'http:':
      title = 'Using HTTP';
      body = 'Your connection to this site is not secure.';
      secondaryBody =
        'You should not enter any sensitive information on this site (passwords, credit cards, etc.) because it could be stolen by attackers.';
      icon = cautionSvg;
      iconHeight = '14';
      iconWidth = '15.5';
      break;
    case 'https:':
      title = 'Using HTTPS';
      body = 'Accessed using a secure traditional connection.';
      secondaryBody = 'Not distributed, but it’ll do I guess.';
      icon = lockSvg;
      iconHeight = '14';
      iconWidth = '12';
      break;
    default:
      const schema = getProtocolSchema(url, protocols);
      const name = schema.name || '';
      title = `Using the ${capitalize(name.replace(':', ''))} protocol`;
      body = schema.description;
      const {asset, ratio} = parseSchemaIcon(schema.icon);
      icon = asset || hexagonSvg;
      let contextOptions = schema.browserContexts || [];
      buttons = contextOptions.filter(ctx => ctx.name !== currentContext);
      iconWidth = asset
        ? ratio
          ? `${ratio * Number(iconHeight)}`
          : '16'
        : '14.5';
      break;
  }
  const bodyMarginLeft = Number(iconWidth) + 8;
  const bubbleRadiusStyle = {
    borderBottomLeftRadius: buttons.length > 0 ? 0 : 7.5,
    borderBottomRightRadius: buttons.length > 0 ? 0 : 7.5,
  };
  return (
    <View style={[styles.container, styles.outterContainer]}>
      <ThemeView
        type="textColor"
        opacity={0.1}
        style={[styles.bubble, bubbleRadiusStyle]}>
        <View
          style={[
            styles.container,
            styles.titleContainer,
            styles.horizontalContainer,
          ]}>
          <ThemeSvg
            width={iconWidth}
            height={iconHeight}
            svgXmlData={icon}
            style={styles.iconStyle}
          />
          <ThemeText numberOfLines={1} style={[styles.title, styles.text]}>
            {title}
          </ThemeText>
        </View>
        {body ? (
          <ThemeText
            numberOfLines={5}
            style={[styles.body, styles.text, {marginLeft: bodyMarginLeft}]}>
            {body}
          </ThemeText>
        ) : null}
        {secondaryBody ? (
          <ThemeText
            numberOfLines={5}
            style={[styles.body, styles.text, {marginLeft: bodyMarginLeft}]}>
            {secondaryBody}
          </ThemeText>
        ) : null}
      </ThemeView>
      {buttons.map((button, index) => {
        const roundingStyle = {
          borderBottomLeftRadius: index === buttons.length - 1 ? 7.5 : 0,
          borderBottomRightRadius: index === buttons.length - 1 ? 7.5 : 0,
        };
        const buttonPressed = () => {
          let inputUrl = url;
          if (currentContext === 'site' && button.name === 'files') {
            // Switching from a site view to files view should start in a directory
            let pathnameSplit = pathname.split('/');
            const lastPath = pathnameSplit[pathnameSplit.length - 1];
            const isFile = lastPath.indexOf('.') > 0;
            if (isFile) {
              inputUrl = inputUrl.replace('/' + lastPath, '');
            }
          }
          pushHistory(tabIndex, {inputUrl, request: button.name});
          NavigationService.goBack();
        };
        const {
          asset: buttonIconAsset,
          ratio: buttonIconRatio,
        } = parseSchemaIcon(button.icon);
        const buttonIconHeight = '14.5';
        return (
          <TouchableOpacity key={button.name + index} onPress={buttonPressed}>
            <ThemeView
              type="highlightColor"
              opacity={0.2}
              style={[
                styles.buttonView,
                styles.horizontalContainer,
                roundingStyle,
              ]}>
              {buttonIconAsset ? (
                <ThemeSvg
                  height={buttonIconHeight}
                  width={
                    buttonIconRatio
                      ? `${buttonIconRatio * Number(buttonIconHeight)}`
                      : buttonIconHeight
                  }
                  svgXmlData={buttonIconAsset}
                  style={styles.iconStyle}
                  type="highlightColor"
                />
              ) : null}
              <ThemeText
                type="highlightColor"
                numberOfLines={1}
                style={[styles.text, styles.title]}>
                {`Open as ${capitalize(button.name)}`}
              </ThemeText>
            </ThemeView>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  outterContainer: {
    margin: 15,
    marginTop: 0,
  },
  bubble: {
    padding: 15,
    paddingBottom: 10,
    borderRadius: 7.5,
  },
  horizontalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    marginBottom: 5,
  },
  text: {
    fontSize: 18,
  },
  title: {
    ...systemWeights.semibold,
    marginLeft: 8,
  },
  body: {
    ...systemWeights.light,
    marginBottom: 5,
  },
  buttonView: {
    borderRadius: 7.5,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    padding: 15,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    protocols: state.protocols,
    tabHistory: state.tabHistory,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProtocolDescription);
