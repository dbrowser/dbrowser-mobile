/**
 * @format
 * @flow
 */

// ARCHIVED - Replaced with Dashboard

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const {List} = require('immutable');

import PopupModalWindow from '../views/PopupModalWindow';
import PopupModalList from '../views/PopupModalList';
import PopupModalListItem from '../views/PopupModalListItem';
import NavigationService from '../lib/NavigationService';
import {ActionCreators} from '../actions';
import {scanQRSvg, createNodeSvg} from '../svg';

const OmniOptionsPopup = ({tabIndex, deviceDimensions}: props) => {
  const orientation = deviceDimensions.get('orientation');
  const screenRatio = orientation === 'landscape' ? 1 / 3.5 : 2 / 3.2;
  let listData = List(['Scan QR Code', 'Create New Node']);

  const scanQR = () => {
    NavigationService.replace({
      name: 'QRScanner',
      params: {
        tabIndex,
      },
    });
  };

  const createDat = () => {};

  const renderItem = ({item, index}) => {
    const maxIndex = listData.size - 1;
    return (
      <PopupModalListItem
        index={index}
        maxIndex={maxIndex}
        title={item}
        icon={index === 0 ? scanQRSvg : createNodeSvg}
        iconType="highlightColor"
        iconSize={index === 0 ? '24' : '26'}
        titleType="highlightColor"
        onPress={index === 0 ? scanQR : createDat}
        numberOfLines={1}
      />
    );
  };

  const keyExtractor = (item, index) => {
    return item + index;
  };

  return (
    <PopupModalWindow screenRatio={screenRatio} omniButton={true}>
      <PopupModalList
        screenRatio={screenRatio}
        immutableData={listData}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </PopupModalWindow>
  );
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OmniOptionsPopup);
