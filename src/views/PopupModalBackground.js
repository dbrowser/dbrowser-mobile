/**
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import NavigationService from '../lib/NavigationService';
import ThemeView from '../views/ThemeView';
import ThemeSvg from '../views/ThemeSvg';
import {hexagonSvg, bigXSvg} from '../svg';
import {ActionCreators} from '../actions';

const PopupModalBackground = ({
  deviceDimensions,
  navButtonPosition,
  button,
  children,
}: props) => {
  const screenWidth = deviceDimensions.get('width');
  const screenHeight = deviceDimensions.get('height');
  const orientation = deviceDimensions.get('orientation');
  const isPortrait = orientation === 'portrait';
  const halfScreenWidth = screenWidth / 2;
  const halfScreenHeight = screenHeight / 2;
  const buttonX = button + 'X';
  const buttonY = button + 'Y';
  const originX = navButtonPosition.get(buttonX);
  const originY = navButtonPosition.get(buttonY);
  const alignItems =
    button === 'omni'
      ? isPortrait
        ? 'center'
        : 'flex-end'
      : originX >= halfScreenWidth
      ? 'flex-end'
      : 'flex-start';
  const justifyContent =
    button === 'omni' || originY >= halfScreenHeight
      ? 'flex-end'
      : 'flex-start';
  const positionStyle = {
    position: 'absolute',
    top: originY,
    left: originX,
  };
  return (
    <ThemeView type="textColor" style={styles.background} opacity={0.2}>
      {button === 'omni' ? (
        <View style={[styles.omniContainer, positionStyle]}>
          <ThemeSvg height="35" width="30" svgXmlData={hexagonSvg} />
          <View style={styles.iconContainer}>
            <ThemeSvg
              type="backgroundColor"
              height="12"
              width="12"
              svgXmlData={bigXSvg}
            />
          </View>
        </View>
      ) : (
        <ThemeView
          type="textColor"
          opacity={0.2}
          style={[styles.buttonOverlay, positionStyle]}
          pointerEvents="none"
        />
      )}
      <SafeAreaView style={[styles.safeView, {alignItems, justifyContent}]}>
        <TouchableOpacity
          style={styles.button}
          activeOpacity={1.0}
          onPress={NavigationService.goBack}
        />
        {children}
      </SafeAreaView>
    </ThemeView>
  );
};

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  background: {
    flex: 1,
  },
  button: {
    ...StyleSheet.absoluteFillObject,
  },
  buttonOverlay: {
    width: 40,
    height: 40,
    borderRadius: 5,
  },
  omniContainer: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
    navButtonPosition: state.navButtonPosition,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopupModalBackground);
