/**
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import AnimatedThemeView from '../views/AnimatedThemeView';
import ThemeSvg from '../views/ThemeSvg';
import {hexagonSvg, bigXSvg} from '../svg';
import {ActionCreators} from '../actions';

const SlideUpModalBackground = ({
  deviceDimensions,
  navButtonPosition,
  dashboard,
  animatedY,
  children,
  onPress,
}: props) => {
  const screenHeight = deviceDimensions.get('height');
  const originX = navButtonPosition.get('omniX');
  const originY = navButtonPosition.get('omniY');
  const positionStyle = {
    position: 'absolute',
    top: originY,
    left: originX,
  };
  const opacity = animatedY.interpolate({
    inputRange: [0, screenHeight],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });
  return (
    <AnimatedThemeView
      type="textColor"
      style={[styles.background, {opacity}]}
      opacity={0.2}>
      {dashboard ? (
        <View style={[styles.omniContainer, positionStyle]}>
          <ThemeSvg height="35" width="30" svgXmlData={hexagonSvg} />
          <View style={styles.iconContainer}>
            <ThemeSvg
              type="backgroundColor"
              height="12"
              width="12"
              svgXmlData={bigXSvg}
            />
          </View>
        </View>
      ) : null}
      <View style={styles.cardContainer}>
        <TouchableOpacity
          style={styles.button}
          activeOpacity={1.0}
          onPress={onPress}
        />
        {children}
      </View>
    </AnimatedThemeView>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
  },
  background: {
    flex: 1,
  },
  button: {
    ...StyleSheet.absoluteFillObject,
  },
  omniContainer: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
    navButtonPosition: state.navButtonPosition,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SlideUpModalBackground);
