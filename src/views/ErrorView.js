/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {systemWeights} from 'react-native-typography';

import ThemeView from './ThemeView';
import ThemeText from './ThemeText';

const errorEmojis = ['😵', '😧', '😢', '🤬', '🦖', '🐛', '⚠️'];

// TODO L - Watchlist for hyper that isn't found
const ErrorView = ({
  tabError,
  tabIndex,
  tabHistory,
  deviceDimensions,
  navigationMeasurements,
}: props) => {
  const [contentHeight, setContentHeight] = useState(0);
  const [emoji, setEmoji] = useState(
    errorEmojis[Math.floor(Math.random() * errorEmojis.length)],
  );
  useEffect(() => {
    setEmoji(errorEmojis[Math.floor(Math.random() * errorEmojis.length)]);
  }, [tabError]);
  const onLayout = ({
    nativeEvent: {
      layout: {height},
    },
  }) => {
    setContentHeight(height);
  };
  const getBodyHeight = () => {
    const deviceHeight = deviceDimensions.get('height');
    const orientation = deviceDimensions.get('orientation');
    const topBarHeight = navigationMeasurements.get('topBarHeight');
    const bottomBarHeight = navigationMeasurements.get('bottomBarHeight');
    const usedBottomBarHeight =
      orientation === 'landscape' ? 0 : bottomBarHeight;
    const listScreenDiff = topBarHeight + usedBottomBarHeight;
    return deviceHeight - listScreenDiff;
  };
  // TODO M - 'Add to watchlist' button for dat errors
  const error = tabError.get(tabIndex);
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const currentPage = history.get(currentIndex);
  if (
    !error ||
    !error.source ||
    !currentPage.inputUrl === error.source.inputUrl ||
    !currentPage.uri === error.source.uri
  ) {
    return null;
  }
  const marginTop = navigationMeasurements.get('topBarHeight');
  const width = deviceDimensions.get('width');
  const textWidth = width - 100;
  const height = getBodyHeight();
  const code = error.statusCode || error.code || null;
  const categoryBottomMargin = code ? 0 : 5;
  const bodyContentHeight = Math.max(contentHeight, height);
  return (
    <ThemeView style={{width, height, marginTop}}>
      <ThemeView type="textColor" opacity={0.15} style={{width, height}}>
        <ScrollView
          style={[styles.scrollView, {width, height}]}
          contentContainerStyle={[
            styles.scrollContainer,
            {height: bodyContentHeight},
          ]}>
          <View
            style={[styles.scrollView, styles.scrollContainer]}
            onLayout={onLayout}>
            <Text
              style={[styles.title, {width: textWidth}]}
              allowFontScaling={false}>
              {emoji}
            </Text>
            <ThemeText
              style={[
                styles.title,
                {width: textWidth, marginBottom: categoryBottomMargin},
              ]}>
              {error.category}
            </ThemeText>
            {code ? (
              <ThemeText style={[styles.title, {width: textWidth}]}>
                {'Code: ' + code}
              </ThemeText>
            ) : null}
            {error.domain ? (
              <ThemeText style={[styles.subtitle, {width: textWidth}]}>
                {error.domain}
              </ThemeText>
            ) : null}
            <ThemeText style={[styles.body, {width: textWidth}]}>
              {error.description}
            </ThemeText>
          </View>
        </ScrollView>
      </ThemeView>
    </ThemeView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'transparent',
  },
  scrollContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    ...systemWeights.bold,
    marginBottom: 5,
    fontSize: 38,
  },
  subtitle: {
    ...systemWeights.semibold,
    marginBottom: 5,
    fontSize: 28,
  },
  body: {
    ...systemWeights.regular,
    marginTop: 5,
    fontSize: 24,
  },
});

function mapStateToProps(state) {
  return {
    navigationMeasurements: state.navigationMeasurements,
    deviceDimensions: state.deviceDimensions,
    tabHistory: state.tabHistory,
    tabError: state.tabError,
  };
}

export default connect(mapStateToProps)(ErrorView);
