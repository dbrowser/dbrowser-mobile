/**
 * @format
 * @flow
 */

import React from 'react';
import SegmentedControl from '@react-native-community/segmented-control';
import {connect} from 'react-redux';

const ThemeSegment = ({theme, style, ...props}: props) => {
  const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
  const backgroundColor = theme.get(subState).get('backgroundColor');
  const textColor = theme.get(subState).get('textColor');
  return (
    <SegmentedControl
      {...props}
      textColor={textColor}
      activeTextColor={backgroundColor}
      tintColor={textColor}
      backgroundColor={textColor.replace('1.0', '0.15')}
      style={style}
    />
  );
};

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(ThemeSegment);
