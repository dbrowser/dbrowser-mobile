/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StatusBar, SafeAreaView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const {Map} = require('immutable');

import {ActionCreators} from '../actions';
import DirectoryPathList from './DirectoryPathList';
import ContentTextView from './ContentTextView';
import SiteInfoHeader from './SiteInfoHeader';
import ContentsList from './ContentsList';
import ThemeView from './ThemeView';

class FilesExplorer extends Component {
  constructor() {
    super();
    this.webView = null;
    this.state = {
      headerHeight: 0,
      directoryListHeight: 0,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      !this.props.navigationMeasurements.equals(
        nextProps.navigationMeasurements,
      )
    ) {
      return true;
    }
    if (!this.props.deviceDimensions.equals(nextProps.deviceDimensions)) {
      return true;
    }
    if (this.state.headerHeight !== nextState.headerHeight) {
      return true;
    }
    if (this.state.directoryListHeight !== nextState.directoryListHeight) {
      return true;
    }
    const tabIndex = nextProps.tabIndex;

    const lastTabError = this.props.tabError.get(tabIndex);
    const nextTabError = nextProps.tabError.get(tabIndex);
    if (!lastTabError || !nextTabError) {
      return false;
    }
    const lastErrorMap = Map(lastTabError);
    const nextErrorMap = Map(nextTabError);
    if (!lastErrorMap.equals(nextErrorMap)) {
      return true;
    }

    const lastTabHistory = this.props.tabHistory.get(tabIndex);
    const nextTabHistory = nextProps.tabHistory.get(tabIndex);
    if (!lastTabHistory || !nextTabHistory) {
      return false;
    }
    const {currentIndex: lastIndex, history: lastHistory} = lastTabHistory;
    const {currentIndex: nextIndex, history: nextHistory} = nextTabHistory;
    const nextSource = nextHistory.get(nextIndex);
    if (nextSource && nextSource.request) {
      return false;
    }
    // If you navBack or navForward in tabHistory
    if (lastIndex !== nextIndex) {
      return true;
    }
    // If tabHistory itself changes
    if (!lastHistory.equals(nextHistory)) {
      return true;
    }
    return false;
  }

  setHeaderHeight = height => {
    this.setState({headerHeight: height});
  };

  setDirectoryListHeight = height => {
    this.setState({directoryListHeight: height});
  };

  getBodyHeight = () => {
    const {
      props: {navigationMeasurements, deviceDimensions},
      state: {headerHeight, directoryListHeight},
    } = this;
    const deviceHeight = deviceDimensions.get('height');
    const orientation = deviceDimensions.get('orientation');
    const topBarHeight = navigationMeasurements.get('topBarHeight');
    const bottomBarHeight = navigationMeasurements.get('bottomBarHeight');
    const usedBottomBarHeight =
      orientation === 'landscape' ? 0 : bottomBarHeight;
    let listScreenDiff =
      topBarHeight + usedBottomBarHeight + headerHeight + directoryListHeight;
    if (Platform.OS === 'android') {
      const statusBarHeight = StatusBar.statusBarHeight || 24;
      listScreenDiff += statusBarHeight;
    }
    return deviceHeight - listScreenDiff;
  };

  render() {
    const {
      getBodyHeight,
      setHeaderHeight,
      setDirectoryListHeight,
      props: {tabError, tabIndex, tabHistory, navigationMeasurements},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    const source = history.get(currentIndex);
    if (source.context !== 'files') {
      return null;
    }
    const currentError = tabError.get(tabIndex);
    if (
      currentError &&
      currentError.source &&
      source.uri === currentError.source.uri &&
      source.inputUrl === currentError.source.inputUrl &&
      source.context === currentError.source.context &&
      source.content === currentError.source.content
    ) {
      return null;
    }
    const marginTop = navigationMeasurements.get('topBarHeight');
    const bodyHeight = getBodyHeight();
    return (
      <ThemeView style={[styles.container, {marginTop}]}>
        <ThemeView type="textColor" opacity={0.1} style={styles.container}>
          <SafeAreaView>
            <SiteInfoHeader tabIndex={tabIndex} onHeight={setHeaderHeight} />
          </SafeAreaView>
          <ThemeView type="textColor" opacity={0.1}>
            <SafeAreaView>
              <DirectoryPathList
                tabIndex={tabIndex}
                onHeight={setDirectoryListHeight}
                contents={source.contents}
                stat={source.stat}
              />
              {source.contents ? (
                <ContentsList
                  tabIndex={tabIndex}
                  url={source.inputUrl}
                  contents={source.contents}
                  height={bodyHeight}
                />
              ) : null}
              {source.content ? (
                <ContentTextView content={source.content} height={bodyHeight} />
              ) : null}
            </SafeAreaView>
          </ThemeView>
        </ThemeView>
      </ThemeView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
  },
  clearWrapper: {
    backgroundColor: 'transparent',
  },
  input: {
    color: 'black',
    backgroundColor: 'white',
    padding: 15,
  },
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps(state) {
  return {
    navigationMeasurements: state.navigationMeasurements,
    deviceDimensions: state.deviceDimensions,
    tabHistory: state.tabHistory,
    tabError: state.tabError,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FilesExplorer);
