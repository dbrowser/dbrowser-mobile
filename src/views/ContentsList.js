/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';
import ActionSheet from 'react-native-action-sheet';
const moment = require('moment');

import {ActionCreators} from '../actions';
import {formatSizeString, getInfoKey} from '../lib/common';
import {shareFileContent} from '../lib/proxy';
import {folderSvg, fileSvg} from '../svg';
import ThemeView from './ThemeView';
import ThemeText from './ThemeText';
import ThemeSvg from './ThemeSvg';

const ContentsList = ({
  tabIndex,
  url,
  contents,
  pushHistory,
  setTabError,
  siteInfo,
  height,
  theme,
}: props) => {
  const [listWidth, setListWidth] = useState(0);
  const textWidth = listWidth - 60;
  const onLayout = ({
    nativeEvent: {
      layout: {width},
    },
  }) => {
    setListWidth(width);
  };
  return (
    <FlatList
      data={contents}
      onLayout={onLayout}
      style={[styles.list, {height}]}
      ListEmptyComponent={() => {
        return (
          <View style={[styles.list, styles.empty, {height, width: listWidth}]}>
            <ThemeText opacity={0.6} numberOfLines={3} style={styles.emptyText}>
              Empty Directory
            </ThemeText>
          </View>
        );
      }}
      renderItem={({item, index}) => {
        const name = item.name || item;
        const size = item.stat ? item.stat.size : undefined;
        const offset = item.stat ? item.stat.offset : undefined;
        const ctime = item.stat ? item.stat.ctime : undefined;
        const marginTop = index === 0 ? 5 : 0;
        // TODO M - Show unique icon for images and videos
        let isFolder = name.indexOf('.') === -1;
        if (name.indexOf('.') === 0) {
          const testString = name.replace('.');
          isFolder =
            testString.indexOf('.') === -1 &&
            (offset === undefined || offset === 0);
        }
        let description;
        if (item.stat) {
          const dateString = moment(ctime).format('M/D/YY');
          const timeString = moment(ctime).format('h:mma');
          const dateTimeString = `${dateString} at ${timeString}`;
          const sizeString = formatSizeString(size);
          description = isFolder
            ? `${dateTimeString}`
            : `${sizeString} - ${dateTimeString}`;
        }
        const onContentPress = () => {
          if (isFolder) {
            const slashString = url.endsWith('/') ? '' : '/';
            const newUrl = url + slashString + name;
            pushHistory(tabIndex, {inputUrl: newUrl, request: 'files'});
          } else {
            const subState = theme.get('chameleonMode')
              ? 'chameleon'
              : 'default';
            const highlightColor = theme.get(subState).get('highlightColor');
            const buttonOptions = ['Open with...'];
            if (
              name.search(
                /\.(css|csv|x?html?|ics|m?js|txt|xml|svg|rtf|sh|jsonl?d?|bin)/i,
              ) !== -1
            ) {
              buttonOptions.unshift('View Text');
            }
            if (name.endsWith('.html')) {
              buttonOptions.unshift('View Page');
            } else {
              buttonOptions.unshift('View File');
            }
            if (Platform.OS === 'ios') {
              buttonOptions.push('Cancel');
            }
            const CANCEL_INDEX = buttonOptions.length;

            ActionSheet.showActionSheetWithOptions(
              {
                options: buttonOptions,
                cancelButtonIndex: CANCEL_INDEX,
                tintColor: highlightColor,
                title: 'File Options',
              },
              async buttonIndex => {
                if (
                  buttonIndex === CANCEL_INDEX ||
                  buttonIndex === 'undefined' ||
                  buttonIndex === undefined
                ) {
                  return;
                }
                const selection = buttonOptions[buttonIndex];
                const slashString = url.endsWith('/') ? '' : '/';
                const newUrl = url + slashString + name;
                if (selection === 'View Page' || selection === 'View File') {
                  pushHistory(tabIndex, {inputUrl: newUrl, request: 'site'});
                } else if (selection === 'View Text') {
                  pushHistory(tabIndex, {inputUrl: newUrl, request: 'files'});
                } else if (selection === 'Open with...') {
                  // Native Share
                  // TODO H - BUG: Sharing '.md' file caused error:
                  //        - "JSON value '{}' of type NSMutableDictionary cannot be converted to NSString"
                  const infoKey = getInfoKey(newUrl);
                  let info =
                    infoKey && siteInfo.get(infoKey)
                      ? siteInfo.get(infoKey).info
                      : undefined;
                  shareFileContent(newUrl, setTabError, info);
                }
              },
            );
          }
        };
        // TODO H - Treat '.goto' files as navigation links
        // TODO H - Render mounts as folder with some sort of shortcut/link indicator
        return (
          <TouchableOpacity
            activeOpacity={0.7}
            style={[styles.touchable, {marginTop}]}
            onPress={onContentPress}>
            <ThemeView style={styles.itemContainer}>
              <View style={styles.iconContainer}>
                <ThemeSvg
                  width={isFolder ? '20' : '15'}
                  height={isFolder ? '16' : '20'}
                  svgXmlData={isFolder ? folderSvg : fileSvg}
                />
              </View>
              <View style={styles.textContainer}>
                <ThemeText style={[styles.name, {width: textWidth}]}>
                  {name}
                </ThemeText>
                {description ? (
                  <ThemeText style={[styles.description, {width: textWidth}]}>
                    {description}
                  </ThemeText>
                ) : null}
              </View>
            </ThemeView>
          </TouchableOpacity>
        );
      }}
      keyExtractor={(item, index) => {
        const name = item.name || item;
        return name + index;
      }}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    backgroundColor: 'transparent',
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchable: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10,
  },
  itemContainer: {
    flexDirection: 'row',
    borderRadius: 7.5,
  },
  iconContainer: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
  },
  textContainer: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingRight: 10,
  },
  emptyText: {
    ...systemWeights.semibold,
    textAlign: 'center',
    marginHorizontal: 60,
    fontSize: 24,
  },
  name: {
    ...systemWeights.semibold,
    marginBottom: 5,
    fontSize: 17,
    flex: 1,
  },
  description: {
    ...systemWeights.light,
    fontSize: 14,
    flex: 1,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
    siteInfo: state.siteInfo,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContentsList);
