/**
 * @format
 * @flow
 */

import React, {useRef, useState} from 'react';
import {View, Image, Platform, StyleSheet} from 'react-native';
import ViewShot from 'react-native-view-shot';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import CustomWebView from '../views/CustomWebView';
import FilesExplorer from '../views/FilesExplorer';
import ErrorView from '../views/ErrorView';
import ThemeView from '../views/ThemeView';
import LoadingBar from '../views/LoadingBar';

import {getImagePalette} from '../lib/common';
import {ActionCreators} from '../actions';

const PageScreen = ({tabIndex, setThemeChameleon}: props) => {
  const [screenshot, setShot] = useState(undefined);
  const viewShot = useRef(null);
  const updatePalette = async backgroundColor => {
    try {
      let imagePath = await viewShot.current.capture();
      // setShot('file://' + imagePath);
      console.log('Palette Screenshot:', imagePath);
      getImagePalette({
        path: imagePath,
      }).then(result => {
        console.log('Palette Result:', result);
        setThemeChameleon({
          ...result,
          backgroundColor,
        });
      });
    } catch (error) {
      console.log('Palette Error:', error);
    }
  };
  const SCREENSHOT_IMAGE_SIZE = Platform.OS === 'android' ? 1275 : 2550;
  // TODO L - Pull down to refresh
  // TODO L - Top and Bottom bars responding to WebView scrolling
  // TODO L - Swipe left/right to navigate back and forward in tabHistory
  // TODO M - Popup at top of screen for app redirect URLs (use RN's canOpenUrl Linking library to determine if the current url can be redirected a RedirectToAppView placed here)
  // TODO H - Popup at top of screen for if there's a new version (tapping causes refresh)
  return (
    <View style={styles.background} collapsable={false}>
      <ThemeView style={StyleSheet.absoluteFillObject} />
      <ErrorView tabIndex={tabIndex} updatePalette={updatePalette} />
      <FilesExplorer tabIndex={tabIndex} updatePalette={updatePalette} />
      <CustomWebView tabIndex={tabIndex} updatePalette={updatePalette} />
      <LoadingBar tabIndex={tabIndex} />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'black',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PageScreen);
