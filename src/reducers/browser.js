import {createReducer} from 'redux-immutablejs';
import {Platform} from 'react-native';
const {Map, List} = require('immutable');
// import CookieManager from '@react-native-community/cookies';

// if (Platform.OS === 'ios') {
//   CookieManager.getAll(/* useWebKit: */ true).then(res => {
//     console.log('CookieManager.getAll', res);
//   });
// }

export const browserInputSettings = createReducer(
  Map({searchEngine: 'DuckDuckGo'}),
  {
    ['SET_SEARCH_ENGINE'](state, action) {
      return state.set('searchEngine', action.engine);
    },
  },
);

export const tabManagement = createReducer(Map({currentTab: 0}), {
  ['SET_CURRENT_TAB'](state, action) {
    return state.set('currentTab', action.index);
  },
});

export const tabHistory = createReducer(
  List([
    {
      currentIndex: 0,
      history: List([{uri: 'https://duckduckgo.com/'}]),
    },
  ]),
  {
    ['SETUP_TAB_HISTORY'](state, action) {
      return action.newHistory;
    },
    ['PUSH_HISTORY'](state, action) {
      const {currentIndex, history} = state.get(action.tabIndex);
      const newTabHistory = history
        .splice(0, currentIndex)
        .insert(0, action.source);
      return state.set(action.tabIndex, {
        currentIndex: 0,
        history: newTabHistory,
      });
    },
    ['REPLACE_HISTORY'](state, action) {
      const {currentIndex, history} = state.get(action.tabIndex);
      const offset = action.offset || 0;
      const replaceIndex = action.index || currentIndex + offset;
      const newTabHistory = history.set(replaceIndex, action.source);
      console.log(
        '[BROWSER] REPLACE_HISTORY return currentIndex',
        currentIndex,
      );
      console.log('[BROWSER] REPLACE_HISTORY return action', action);
      console.log(
        '[BROWSER] REPLACE_HISTORY return newTabHistory',
        newTabHistory,
      );
      console.log('[BROWSER] REPLACE_HISTORY return state', state);
      return state.set(action.tabIndex, {
        currentIndex,
        history: newTabHistory,
      });
    },
    ['NAV_FORWARD'](state, action) {
      let {currentIndex, history} = state.get(action.tabIndex);
      if (currentIndex > 0 && currentIndex - action.iterations >= 0) {
        currentIndex -= action.iterations;
      }
      return state.set(action.tabIndex, {
        currentIndex,
        history,
      });
    },
    ['NAV_BACK'](state, action) {
      let {currentIndex, history} = state.get(action.tabIndex);
      if (
        currentIndex < history.size - 1 &&
        currentIndex + action.iterations <= history.size - 1
      ) {
        currentIndex += action.iterations;
      }
      return state.set(action.tabIndex, {
        currentIndex,
        history,
      });
    },
  },
);

export const browserCommand = createReducer(
  List([{key: 0, action: '', iterations: 1}]),
  {
    ['NAV_FORWARD_CMD'](state, action) {
      return state.set(action.tabIndex, {
        key: Math.random(),
        action: 'goForward',
        iterations: action.iterations,
      });
    },
    ['NAV_BACK_CMD'](state, action) {
      return state.set(action.tabIndex, {
        key: Math.random(),
        action: 'goBack',
        iterations: action.iterations,
      });
    },
    ['RELOAD'](state, action) {
      return state.set(action.tabIndex, {
        key: Math.random(),
        action: 'reload',
        iterations: 1,
      });
    },
    ['STOP_LOADING'](state, action) {
      return state.set(action.tabIndex, {
        key: Math.random(),
        action: 'stopLoading',
        iterations: 1,
      });
    },
  },
);

export const tabError = createReducer(List([{}]), {
  ['SET_TAB_ERROR'](state, action) {
    return state.set(action.tabIndex, action.error);
  },
  ['PUSH_HISTORY'](state, action) {
    return state.set(action.tabIndex, {});
  },
  ['REPLACE_HISTORY'](state, action) {
    return state.set(action.tabIndex, {});
  },
  ['NAV_FORWARD'](state, action) {
    return state.set(action.tabIndex, {});
  },
  ['NAV_BACK'](state, action) {
    return state.set(action.tabIndex, {});
  },
});

export const canGoBack = createReducer(List([false]), {
  ['SET_CAN_GO_BACK'](state, action) {
    return state.set(action.tabIndex, action.bool);
  },
});

export const canGoForward = createReducer(List([false]), {
  ['SET_CAN_GO_FORWARD'](state, action) {
    return state.set(action.tabIndex, action.bool);
  },
});

export const loading = createReducer(List([0]), {
  ['SET_LOADING_PROGRESS'](state, action) {
    return state.set(action.tabIndex, action.progress);
  },
  ['SET_TAB_ERROR'](state, action) {
    return state.set(action.tabIndex, 1);
  },
  ['REPLACE_HISTORY'](state, action) {
    return state.set(action.tabIndex, 1);
  },
});

export const webViewInjection = createReducer(Map({}), {
  ['SET_ROOT_INJECTION'](state, action) {
    // Root can't be overwritten once set
    if (!state.get('root')) {
      return state.set('root', action.script);
    }
    return state;
  },
});
