// SECTION - TOAST AND NOTIFICATIONS

// Show toast message
export function showToast(message) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_TOAST_MESSAGE',
      message,
    });
  };
}

// SECTION - THEME FUNCTIONS

// Set backgroundColor, textColor, or highlightColor
export function setThemeColor(category, color, url) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_THEME_COLOR',
      category,
      color,
      url,
    });
  };
}

// Set chameleon mode
export function setThemeChameleon(theme) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_THEME_CHAMELEON',
      theme,
    });
  };
}

// Toggle chameleon mode
export function toggleThemeChameleonMode(url) {
  return (dispatch, getState) => {
    dispatch({
      type: 'TOGGLE_THEME_CHAMELEON_MODE',
      url,
    });
  };
}

// SECTION - MEASUREMENT FUNCTIONS

// Set measured height of top navigation bar
export function setTopBarHeight(height) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_TOP_BAR_HEIGHT',
      height,
    });
  };
}

// Set measured height of bottom navigation bar
export function setBottomBarHeight(height) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_BOTTOM_BAR_HEIGHT',
      height,
    });
  };
}

// Set measured position of navigation button
export function setNavButtonPosition(isBack, x, y) {
  return (dispatch, getState) => {
    let type = 'SET_FWD_BUTTON_POSITION';
    if (isBack) {
      type = 'SET_BACK_BUTTON_POSITION';
    }
    dispatch({
      type,
      x,
      y,
    });
  };
}

// Set measured position of omni button
export function setOmniButtonPosition(x, y) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_OMNI_BUTTON_POSITION',
      x,
      y,
    });
  };
}

// Set measured position of options button
export function setOptButtonPosition(x, y) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_OPT_BUTTON_POSITION',
      x,
      y,
    });
  };
}

// Set device screen size and orientation
export function setDimensions({orientation, height, width}) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_DEVICE_DIMENSIONS',
      orientation,
      height,
      width,
    });
  };
}

export function startBrowser() {
  return (dispatch, getState) => {
    dispatch({
      type: 'START_BROWSER',
    });
  };
}
