const basicParseURL = require('url-parse');
const {Map} = require('immutable');
var RNFS = require('react-native-fs');
import markdownToHtml from '../backend/lib/markdown/api';

import {
  getInfoKey,
  getProtocolSchema,
  dispatchBrowserError,
  maybeResetChameleonTheme,
} from '../lib/common';
// import {pushFetch} from '../lib/proxy-regenerated';

export function setBrowserSearchEngine(engine) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_SEARCH_ENGINE',
      engine,
    });
  };
}

// TODO L - BUG: DuckDuckGo Redirect (screenshotted) is treated as a separate page
export function pushHistory(tabIndex, source, forceLoad) {
  // TODO M - New tab if tabIndex is undefined
  return async (dispatch, getState) => {
    console.log('[BROWSER] Pushing history', tabIndex, source);
    const {currentIndex, history} = getState().tabHistory.get(tabIndex);
    const currentPage = history.get(currentIndex);
    // const forwardIndex = currentIndex - 1;
    // const backIndex = currentIndex + 1;
    // const forwardPage = history.get(forwardIndex);
    // const backPage = history.get(backIndex);
    const currentUrl =
      currentPage.requestUrl || currentPage.inputUrl || currentPage.uri;
    let newSourceUrl = source.requestUrl || source.inputUrl || source.uri;
    newSourceUrl = newSourceUrl.replace(/gateway:\/\//i, 'hyper://');
    let offset = 0;
    if (
      !forceLoad &&
      !currentPage.request &&
      source.uri === currentPage.uri &&
      source.inputUrl === currentPage.inputUrl &&
      source.requestUrl === currentPage.requestUrl &&
      source.context === currentPage.context &&
      source.contents === currentPage.contents &&
      Map(source.scopeInfo).equals(Map(currentPage.scopeInfo))
    ) {
      return;
    }
    let type = 'PUSH_HISTORY';

    const {host: currentHost, protocol: currentProtocol} = basicParseURL(
      currentUrl,
    );
    const {host: newHost, protocol: newProtocol} = basicParseURL(newSourceUrl);
    if (
      !source.request &&
      currentHost.toLowerCase().replace(/m\.|www\./gi, '') ===
        newHost.toLowerCase().replace(/m\.|www\./gi, '') &&
      Map(source.scopeInfo).equals(Map(currentPage.scopeInfo)) &&
      currentProtocol.toLowerCase().replace(/https?/gi, '') ===
        newProtocol.toLowerCase().replace(/https?/gi, '') &&
      (forceLoad ||
        currentPage.request ||
        (currentPage.context === source.context &&
          currentUrl
            .toLowerCase()
            .replace(/m\.|www\.|https?/gi, '')
            .replace(/\/index\.html/i, '/') ===
            newSourceUrl
              .toLowerCase()
              .replace(/m\.|www\.|https?/gi, '')
              .replace(/\/index\.html/i, '/')))
    ) {
      type = 'REPLACE_HISTORY';
    }
    console.log('[BROWSER] Final push:', type, tabIndex, source);
    const requestUrl = source.requestUrl || source.inputUrl;
    dispatch({
      type,
      tabIndex,
      source:
        forceLoad && requestUrl
          ? {inputUrl: requestUrl, request: true}
          : source,
      offset,
    });
    // Handle requests
    if (source.request || forceLoad) {
      let requestType =
        source.context || source.request || currentPage.context || 'site';
      const schema = getProtocolSchema(requestUrl, getState().protocols);
      if (
        schema &&
        schema.browserContexts &&
        schema.browserContexts.findIndex(
          (context) => context.name === requestType,
        ) === -1
      ) {
        requestType = schema.browserContexts[0].name;
      }
      console.log(
        '[BROWSER] Submitting to node from push:',
        tabIndex,
        source,
        requestType,
      );
      const infoKey = getInfoKey(requestUrl);
      let requestOpts = {
        method: 'LOAD',
        headers: {
          'Site-Info': infoKey ? getState().siteInfo.get(infoKey) : undefined,
          Context: requestType,
          Navigate: true,
        },
      };
      if (typeof tabIndex === 'number') {
        requestOpts.headers['Tab-Index'] = tabIndex;
      }
      // pushFetch(source, requestOpts, offset, dispatch, getState);
      try {
        const fetchResponse = await fetch(requestUrl, requestOpts);
        console.log('[BROWSER] GOT FETCH LOAD RESPONSE:', fetchResponse);
        let result = await fetchResponse.body;
        console.log('[BROWSER] GOT NAV BODY RESULT:', result);
        if (
          result &&
          result.source &&
          result.source.uri &&
          result.source.uri.endsWith('.md')
        ) {
          // Render .md as .html
          const htmlUri = result.source.uri.replace('.md', '-md.html');
          const htmlExists = await RNFS.exists(htmlUri);
          if (!htmlExists) {
            const mdString = await RNFS.readFile(result.source.uri);
            const htmlString = markdownToHtml.toHTML(mdString);
            await RNFS.writeFile(htmlUri, htmlString);
          }
          result.source.uri = htmlUri;
        }
        if (result.source.context !== 'site') {
          maybeResetChameleonTheme({
            dispatch,
            getState,
            tabIndex: result.tabIndex,
            newUrl:
              result.source.requestUrl ||
              result.source.inputUrl ||
              result.source.uri,
          });
        }
        dispatch({
          type: 'REPLACE_HISTORY',
          tabIndex: result.tabIndex,
          source: result.source,
          offset,
        });
      } catch (error) {
        dispatchBrowserError(
          {url: requestUrl, tabIndex, error},
          dispatch,
          getState,
        );
      }
    }
  };
}

export function parseBackForward(tabIndex, source) {
  return (dispatch, getState) => {
    let type;
    console.log('[BROWSER] parseBackForward', tabIndex, source);
    const {currentIndex, history} = getState().tabHistory.get(tabIndex);
    const forwardIndex = currentIndex - 1;
    const backIndex = currentIndex + 1;
    const forwardPage = history.get(forwardIndex);
    const backPage = history.get(backIndex);
    if (forwardIndex >= 0 && forwardPage.uri === source.uri) {
      type = 'NAV_FORWARD';
    } else if (backPage && backPage.uri === source.uri) {
      type = 'NAV_BACK';
    }
    if (!type) {
      return;
    }
    dispatch({
      type,
      tabIndex,
      iterations: 1,
    });
  };
}

// TODO H - Fix /.ui handling for back/fwd navigation
export function navBack(tabIndex, iterations) {
  return async (dispatch, getState) => {
    const {currentIndex, history} = getState().tabHistory.get(tabIndex);
    let backIndex = currentIndex;
    if (
      backIndex < history.size - 1 &&
      backIndex + iterations <= history.size - 1
    ) {
      backIndex += iterations;
    }
    const backPage = history.get(backIndex);
    if (backPage) {
      const requestUrl = backPage.requestUrl || backPage.inputUrl;
      if (requestUrl) {
        dispatch({
          type: 'REPLACE_HISTORY',
          tabIndex,
          source: {
            inputUrl: requestUrl,
            request: backPage.context || 'site',
          },
          index: backIndex,
        });
      }

      dispatch({
        type: 'NAV_BACK',
        tabIndex,
        iterations,
      });

      if (requestUrl) {
        const infoKey = getInfoKey(requestUrl);
        let requestOpts = {
          method: 'LOAD',
          headers: {
            'Site-Info': infoKey ? getState().siteInfo.get(infoKey) : undefined,
            Context: backPage.context || 'site',
            'Tab-Index': tabIndex,
            Navigate: true,
          },
        };
        try {
          const fetchResponse = await fetch(requestUrl, requestOpts);
          const result = await fetchResponse.body;
          console.log('[BROWSER] GOT BACK_NAV RESPONSE:', result);
          // TODO H - Prevent dispatch if the inputUrl is no longer likely the same site (as in hyper://jdskfj/ = hyper://jdskfj/index.html)
          if (
            result &&
            result.source &&
            result.source.uri &&
            result.source.uri.endsWith('.md')
          ) {
            // Render .md as .html
            const htmlUri = result.source.uri.replace('.md', '-md.html');
            const htmlExists = await RNFS.exists(htmlUri);
            if (!htmlExists) {
              const mdString = await RNFS.readFile(result.source.uri);
              const htmlString = markdownToHtml.toHTML(mdString);
              await RNFS.writeFile(htmlUri, htmlString);
            }
            result.source.uri = htmlUri;
          }
          dispatch({
            type: 'REPLACE_HISTORY',
            tabIndex,
            source: result.source,
            index: backIndex,
          });
        } catch (error) {
          dispatchBrowserError(
            {url: requestUrl, tabIndex, error},
            dispatch,
            getState,
          );
        }
      }
    }
  };
}

export function navForward(tabIndex, iterations) {
  return async (dispatch, getState) => {
    const {currentIndex, history} = getState().tabHistory.get(tabIndex);
    let fwdIndex = currentIndex;
    if (fwdIndex > 0 && fwdIndex - iterations >= 0) {
      fwdIndex -= iterations;
    }
    const fwdPage = history.get(fwdIndex);
    if (fwdPage) {
      const requestUrl = fwdPage.requestUrl || fwdPage.inputUrl;
      if (requestUrl) {
        dispatch({
          type: 'REPLACE_HISTORY',
          tabIndex,
          source: {
            inputUrl: requestUrl,
            request: fwdPage.context || 'site',
          },
          index: fwdIndex,
        });
      }

      dispatch({
        type: 'NAV_FORWARD',
        tabIndex,
        iterations,
      });

      if (requestUrl) {
        const infoKey = getInfoKey(requestUrl);
        let requestOpts = {
          method: 'LOAD',
          methodOpts: {
            'Site-Info': infoKey ? getState().siteInfo.get(infoKey) : undefined,
            Context: fwdPage.context || 'site',
            'Tab-Index': tabIndex,
            Navigate: true,
          },
        };
        try {
          const fetchResponse = await fetch(requestUrl, requestOpts);
          const result = await fetchResponse.body;
          console.log('[BROWSER] GOT FWD_NAV RESPONSE:', result);
          if (
            result &&
            result.source &&
            result.source.uri &&
            result.source.uri.endsWith('.md')
          ) {
            // Render .md as .html
            const htmlUri = result.source.uri.replace('.md', '-md.html');
            const htmlExists = await RNFS.exists(htmlUri);
            if (!htmlExists) {
              const mdString = await RNFS.readFile(result.source.uri);
              const htmlString = markdownToHtml.toHTML(mdString);
              await RNFS.writeFile(htmlUri, htmlString);
            }
            result.source.uri = htmlUri;
          }
          // TODO H - Cancel current load and make sure replacement doesn't happen if the user navigated during load. Apply to navBack and navFwd
          dispatch({
            type: 'REPLACE_HISTORY',
            tabIndex,
            source: result.source,
            index: fwdIndex,
          });
        } catch (error) {
          dispatchBrowserError(
            {url: requestUrl, tabIndex, error},
            dispatch,
            getState,
          );
        }
      }
    }
  };
}

export function reloadWebView(tabIndex) {
  return (dispatch, getState) => {
    dispatch({
      type: 'RELOAD',
      tabIndex,
    });
  };
}

export function stopLoadingWebView(tabIndex) {
  return (dispatch, getState) => {
    dispatch({
      type: 'STOP_LOADING',
      tabIndex,
    });
  };
}

export function setCanGoBack(tabIndex, bool) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_CAN_GO_BACK',
      tabIndex,
      bool,
    });
  };
}

export function setCanGoForward(tabIndex, bool) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_CAN_GO_FORWARD',
      tabIndex,
      bool,
    });
  };
}

export function setNavigationProgress({tabIndex, progress}) {
  return (dispatch, getState) => {
    let loadingIndex = tabIndex;
    if (tabIndex === undefined || tabIndex === null) {
      // TODO L - Set to the current tab the user is on
      //        - Likely means changing tabHistory to immutable map with 'currentTab' key, and 'tabs' that contains current tabHistory
      loadingIndex = 0;
    }
    const currentLoading = getState().loading.get(loadingIndex);
    if (progress <= 0.1 || currentLoading < progress) {
      dispatch({
        type: 'SET_LOADING_PROGRESS',
        tabIndex,
        progress,
      });
    }
  };
}

export function setTabError({url, tabIndex, error}) {
  return (dispatch, getState) => {
    dispatchBrowserError({url, tabIndex, error}, dispatch, getState);
  };
}

export function setSiteInfo({url, info, timestamp}) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_SITE_INFO',
      url,
      info,
      timestamp,
    });
  };
}

export function setWebViewInjection(script) {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_ROOT_INJECTION',
      script,
    });
  };
}
