import {StackActions} from 'react-navigation';
import {CommonActions} from '@react-navigation/native';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate({name, params}) {
  _navigator.dispatch(
    CommonActions.navigate({
      name,
      params,
    }),
  );
}

function replace({name, params}) {
  _navigator.dispatch(CommonActions.replace(name, params));
}

function push(name, params) {
  _navigator.dispatch(StackActions.push(name, params));
}

function goBack() {
  _navigator.dispatch(CommonActions.goBack());
}

function popToTop() {
  _navigator.dispatch(StackActions.popToTop());
}

// add other navigation functions that you need and export them

export default {
  push,
  goBack,
  navigate,
  popToTop,
  replace,
  setTopLevelNavigator,
};
