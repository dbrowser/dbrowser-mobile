import nodejs from 'nodejs-mobile-react-native';
const regeneratorRuntime = require('regenerator-runtime');
import {Platform} from 'react-native';
import Share from 'react-native-share';
var RNFS = require('react-native-fs');

import {dispatchBrowserError} from './common';

const parseFetchMessage = (msg, resolve, reject) => {
  if (Array.isArray(msg)) {
    // There are multiple results to listen for
    const res = msg.map((response, responseIndex) => {
      if (response.bodyId) {
        // response.bodyId is the unique ID for body listeners
        const subRequestId = response.bodyId;
        response.body = new Promise((subResolve, subReject) => {
          nodejs.channel.addListener(subRequestId, subMsg => {
            parseFetchMessage(subMsg, subResolve, subReject);
          });
        });
        response.bodyId = undefined;
      }
      return response;
    });
    resolve(res);
    return;
  }
  if (msg.end !== undefined && msg.end !== null) {
    return;
  }
  if (msg.bodyId || (!msg.result && !msg.error)) {
    // This is the status result
    // Add body listener if needed, otherwise assume finished
    if (msg.bodyId) {
      console.log('[BROWSER] Listening to response body:', msg);
      const subRequestId = msg.bodyId;
      msg.body = new Promise((subResolve, subReject) => {
        nodejs.channel.addListener(subRequestId, subMsg => {
          parseFetchMessage(subMsg, subResolve, subReject);
        });
      });
      msg.bodyId = undefined;
    }
    resolve(msg);
    return;
  }
  // This is the body result
  const {iteration, result, error, end} = msg;
  if (error) {
    console.log('[BROWSER] Fetch error:', error);
    reject(error);
  }
  if (result) {
    console.log('[BROWSER] Node Result Returned:', result);
    // TODO M - Async Generators if there's an 'iteration' value
    // let nextIteration = 0;
    // let cache = {};
    // let finished = false;
    // let returns = [];
    // const checkCache = array => {
    //   if (cache[nextIteration]) {
    //     nextIteration += 1;
    //     const caught = cache[nextIteration];
    //     return checkCache(array.concat([caught]));
    //   }
    //   return array;
    // };
    // nodejs.channel.addListener(requestId, ({iteration, result, end}) => {
    //   if (end) {
    //     finished = end;
    //   }
    //   if (iteration && result) {
    //     if (iteration === nextIteration) {
    //       nextIteration += 1;
    //       returns = returns.concat(checkCache([result]));
    //       // For test
    //       finished = true;
    //     } else {
    //       cache[iteration] = result;
    //     }
    //   }
    // });
    // async function* response() {
    //   while (!finished || returns.length > 0) {
    //     for (var j = 0; j < returns.length; j++) {
    //       yield returns[j];
    //     }
    //     returns = [];
    //   }
    // }
    // return response();
    // TODO M - Handle callbacks
    // if (callback) {
    //   callback(result);
    // }
    // NOTE - No Async Generators on React Native's end at the moment
    resolve(result);
  }
};

export const fetchHandler = fetchListener => {
  return {
    apply(target, thisArg, argumentsList) {
      console.log('[BROWSER] Fetching...', argumentsList);
      let isSingleRequest = false;
      if (!Array.isArray(argumentsList[0])) {
        isSingleRequest = true;
        const url = argumentsList[0].url || argumentsList[0];
        if (url && url.search(/https?:\/\//i) !== -1) {
          // Return normal fetch, save precious resources
          return Reflect.apply(target, thisArg, argumentsList);
        }
        if (url && url.search(/file:\/\//i) !== -1) {
          // TODO M - Return fetch query/write on filesystem [GET = read (can set Content-Type to 'application/stat' or something, or have separate STAT method), PUT = write]
        }
      }
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'fetch*' + new Date().getTime() + '*' + randomInt;
      console.log(
        '[BROWSER] Sending fetch to node...',
        requestId,
        argumentsList,
      );
      nodejs.channel.post(fetchListener, {
        requestId,
        argumentsList,
      });
      return new Promise((resolve, reject) => {
        nodejs.channel.addListener(requestId, msg => {
          parseFetchMessage(msg, resolve, reject);
        });
      });
    },
  };
};

export async function shareFileContent(url, setTabError, siteInfo) {
  try {
    const response = await fetch(url, {
      method: 'SHARE',
      headers: {
        'Site-Info': siteInfo,
      },
    });
    const result = await response.body;
    let shareOptions = {
      url: Platform.OS === 'android' ? 'file://' + result : result,
    };
    await Share.open(shareOptions);
  } catch (error) {
    setTabError({url, tabIndex: undefined, error});
  }
}

export async function pushFetch(
  source,
  requestOpts,
  offset,
  dispatch,
  getState,
) {
  try {
    const response = await fetch(source.inputUrl, requestOpts);
    console.log('[BROWSER] pushFetch request:', response);
    for await (const result of response.body) {
      console.log('[BROWSER] GOT LOAD RESPONSE:', result);
      // if (result.source.infoPath) {
      //   RNFS.readFile(result.source.infoPath)
      //     .then(json => {
      //       const info = JSON.parse(json);
      //       dispatch({
      //         type: 'SET_SITE_INFO',
      //         path: result.source.infoPath,
      //         info,
      //       });
      //     })
      //     .catch(e => {
      //       console.log(
      //         'Error getting site info JSON for path:',
      //         result.source.infoPath,
      //         e,
      //       );
      //     });
      // }
      dispatch({
        type: 'REPLACE_HISTORY',
        tabIndex: result.tabIndex,
        source: result.source,
        offset,
      });
    }
  } catch (error) {
    dispatchBrowserError(
      {url: source.inputUrl, tabIndex: requestOpts.tabIndex, error},
      dispatch,
      getState,
    );
  }
}
