function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function () {}; return { s: F, n: function () { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function (e) { throw e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function () { it = o[Symbol.iterator](); }, n: function () { var step = it.next(); normalCompletion = step.done; return step; }, e: function (e) { didErr = true; err = e; }, f: function () { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

import nodejs from 'nodejs-mobile-react-native';

var regeneratorRuntime = require('regenerator-runtime');

var RNFS = require('react-native-fs');

import { getProtocolSchema } from './common';
export var fetchHandler = {
  apply(target, thisArg, argumentsList) {
    console.log('[BROWSER] Fetching...', argumentsList);
    var url = argumentsList[0];

    if (typeof url === 'string' && url.search(/https?:\/\//i) === -1 && url.search(/file:\/\//i) === -1) {
      var _marked = /*#__PURE__*/regeneratorRuntime.mark(response);

      console.log('[BROWSER] Starting fetch response...', url);
      var schema = getProtocolSchema(url);
      var methodType = schema && schema.methods && Object.keys(schema.methods).length > 0 ? Object.keys(schema.methods)[0] : 'GET';
      var fetchOptions = {};
      var callback;

      if (argumentsList[2] && typeof argumentsList[2] === 'function') {
        callback = argumentsList[2];
      } else if (argumentsList[1] && typeof argumentsList[1] === 'function') {
        callback = argumentsList[1];
      } else if (argumentsList[1]) {
        fetchOptions = argumentsList[1];
      }

      if (fetchOptions.method && schema && schema.methods && schema.methods[fetchOptions.method]) {
        methodType = fetchOptions.method;
      }

      var opts = Object.assign(fetchOptions, {
        method: methodType
      });
      var randomInt = Math.floor(Math.random() * Math.floor(100000));
      var requestId = methodType + '*' + new Date().getTime() + '*' + randomInt;
      console.log('[BROWSER] Sending fetch to node...', requestId, url, opts);
      nodejs.channel.post('fetch', {
        requestId,
        url,
        opts
      }); // TODO M - Async Generators

      console.log('[BROWSER] Fetch request sent to node!');
      var nextIteration = 0;
      var cache = {};
      var finished = false;
      var returns = [];

      var checkCache = function (array) {
        if (cache[nextIteration]) {
          nextIteration += 1;
          var caught = cache[nextIteration];
          return checkCache(array.concat([caught]));
        }

        return array;
      };

      nodejs.channel.addListener(requestId, function ({
        iteration,
        result,
        end
      }) {
        if (end) {
          finished = end;
        }

        if (iteration && result) {
          if (iteration === nextIteration) {
            nextIteration += 1;
            returns = returns.concat(checkCache([result])); // For test

            finished = true;
          } else {
            cache[iteration] = result;
          }
        }
      });

      function response() {
        var j;
        return regeneratorRuntime.async(function response$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(!finished || returns.length > 0)) {
                  _context.next = 11;
                  break;
                }

                j = 0;

              case 2:
                if (!(j < returns.length)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return returns[j];

              case 5:
                j++;
                _context.next = 2;
                break;

              case 8:
                returns = [];
                _context.next = 0;
                break;

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _marked, null, null, Promise);
      }

      return response(); // NOTE - No Async Generators. Not ideal, but works
      // return new Promise((resolve, reject) => {
      //   nodejs.channel.addListener(requestId, ({iteration, result, end}) => {
      //     console.log('[BROWSER] Node Result Returned:', result);
      //     if (result) {
      //       if (callback) {
      //         callback({iteration, result, end});
      //       }
      //       resolve({iteration, result, end});
      //     }
      //   });
      // });
    } else {
      return Reflect.apply(target, thisArg, argumentsList);
    }
  }

};
export function pushFetch(dispatch, source, requestOpts, offset) {
  var request, _loop, _iterator, _step, result;

  return regeneratorRuntime.async(function pushFetch$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          request = fetch(source.inputUrl, requestOpts);
          console.log('[BROWSER] pushFetch request:', request);

          _loop = function (result) {
            console.log('[BROWSER] GOT LOAD RESPONSE:', result);

            if (result.source.infoPath) {
              RNFS.readFile(result.source.infoPath).then(function (json) {
                var info = JSON.parse(json);
                dispatch({
                  type: 'SET_SITE_INFO',
                  path: result.source.infoPath,
                  info
                });
              }).catch(function (e) {
                console.log('Error getting site info JSON for path:', result.source.infoPath, e);
              });
            }

            if (result.source.context === 'files' && !result.source.contents) {
              if (result.source.uri) {
              } else {
                result.source.contents = [];
                dispatch({
                  type: 'REPLACE_HISTORY',
                  tabIndex: result.tabIndex,
                  source: result.source,
                  offset
                });
              }
            } else {
              dispatch({
                type: 'REPLACE_HISTORY',
                tabIndex: result.tabIndex,
                source: result.source,
                offset
              });
            }
          };

          _iterator = _createForOfIteratorHelper(request);

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              result = _step.value;

              _loop(result);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

        case 5:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, null, Promise);
}
