// Setup Browser API
require('./lib/setup.js');

// QR Codes
require('./lib/qr.js');

// Color Extraction
require('./lib/color.js');
