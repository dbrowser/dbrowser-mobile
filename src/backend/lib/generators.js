var rn_bridge = require('rn-bridge');
const fs = require('fs');

const {formatError, makeFsExport} = require('./common.js');

// [BABEL IS NO LONGER NEEDED]
// const regeneratorRuntime = require('regenerator-runtime');

// TODO H - Proxy/Modify require so all fs actions are limited to within their allowed TEMP_DIR and PERM_DIR
//        - Block all fs read/writes if there are no dirs passed
//        - Prevent illegal actions like a Hyperdrive modifying another hyperdrive of yours (?)
//        - Would also have to remove any pre-existing attempts at proxying the same functions
function functionFromString({body, /*name,*/ params, paramNames}) {
  let functionString = typeof body === 'string' ? body : `${body}`;

  // HACK - Need to name function because anonymous async functions make babel throw a syntax error [BABEL IS NO LONGER NEEDED]
  // let functionName = name
  //   ? name.toLowerCase().replace(/[ \s]+/g, '')
  //   : 'stringFunction';
  //
  // let functionIndex = functionString.search(/function\*? ?\(/);
  // if (functionIndex !== -1) {
  //   functionString = functionString.replace(/function\*? ?\(/, match => {
  //     return match.replace(/ ?\(/, ` ${functionName}(`);
  //   });
  // } else {
  //   // TODO M - Set functionName that the function already has
  // }

  // Babelify [BABEL IS NO LONGER NEEDED]
  // let transformedCode = functionString;
  // const babelified = require('@babel/core').transform(transformedCode, {
  //   plugins: [
  //     '@babel/plugin-proposal-object-rest-spread',
  //     '@babel/plugin-syntax-typescript',
  //     // '@babel/plugin-transform-destructuring',
  //     '@babel/plugin-transform-parameters',
  //     '@babel/plugin-transform-spread',
  //     'babel-plugin-syntax-object-rest-spread',
  //     'transform-es2015-spread',
  //   ],
  // });
  // transformedCode = babelified.code;

  // Regenerator - [BABEL IS NO LONGER NEEDED]
  // if (functionString.indexOf('async function*') !== -1) {
  //   console.log('[NODE] Regenerator transform...', transformedCode);
  //   // Regenerator
  //   transformedCode = require('regenerator').compile(transformedCode).code;
  //   transformedCode =
  //     "const regeneratorRuntime = require('regenerator-runtime');\n\n" +
  //     transformedCode;
  // }
  // console.log('[NODE] Transformed the code!');

  const returnString = 'return ' + functionString;
  const wrappedString = '{ ' + returnString + ' }';

  let functionParamNames = ['exports', 'require', 'module', '__dirname'];
  if (paramNames) {
    functionParamNames = functionParamNames.concat(paramNames);
  }
  let functionParams = [exports, require, module, __dirname];
  if (params) {
    functionParams = functionParams.concat(params);
  }
  const newFunction = new Function(...functionParamNames, wrappedString)(
    ...functionParams,
  );
  return newFunction;
}

const fetchHandler = (protocols, apis) => {
  return {
    apply(target, thisArg, argumentsList) {
      console.log('[NODE] Fetching...', argumentsList);
      let urls = argumentsList[0];
      if (!Array.isArray(urls)) {
        if (typeof urls === 'string') {
          urls = [argumentsList];
        } else {
          urls = [urls];
        }
      }
      let requestPromises = [];
      for (const urlIndex in urls) {
        let request = urls[urlIndex];
        if (Array.isArray(request)) {
          const fallbackOpts =
            argumentsList[1] && typeof argumentsList[1] === 'object'
              ? argumentsList[1]
              : {};
          const opts = request[1] || fallbackOpts;
          request = {url: request[0], ...opts};
        } else if (typeof request === 'string') {
          const opts =
            argumentsList[1] && typeof argumentsList[1] === 'object'
              ? argumentsList[1]
              : {};
          request = {url: request, ...opts};
        }
        if (
          request.url &&
          request.url.search(/https?:\/\//i) === -1 &&
          request.url.search(/file:\/\//i) === -1
        ) {
          let gotResponse = false;
          // TODO M - Handle conflicting protocols (prefixes match)
          for (var i = 0; i < protocols.length; i++) {
            const protocol = protocols[i];
            let protocolName = protocol.name;
            if (typeof protocol === 'string') {
              // TODO H - Get JSON from reference
            }
            const protocolPrefix = protocol.prefix;
            const prefixRegex = new RegExp(protocolPrefix);
            if (prefixRegex.test(request.url)) {
              let methodType =
                protocol.methods && Object.keys(protocol.methods).length > 0
                  ? Object.keys(protocol.methods)[0]
                  : 'GET';
              if (request.method && protocol.methods[request.method]) {
                methodType = request.method;
              }
              request.method = methodType;
              if (protocol.methods && protocol.methods[methodType]) {
                // Create Response
                // TODO H - Parse URL (figure out async behavior -- IDEA: Get number of expected responses, don't return until that number of parses have gone through using .then()),
                //        - Create PERM_DIR and TEMP_DIR based on protocol's instanceId (defaults to hostname if there is none),
                //        - Pass PERM_DIR, TEMP_DIR, and PARSED_URL to functionFromString
                const response = {
                  body: functionFromString({
                    body: protocol.methods[methodType],
                    name: `${methodType}${protocolName}`,
                    params: [makeFsExport(apis)],
                    paramNames: ['EXPORT_TO_FILESYSTEM'],
                  })(request),
                  status: 200,
                  statusText: 'OK',
                };
                // TODO M - Replicate expected body spec: https://developer.mozilla.org/en-US/docs/Web/API/Body
                //        - Mauve also did a dat-fetch implementation: https://github.com/RangerMauve/dat-fetch/blob/master/index.js
                requestPromises.push(response);
                gotResponse = true;
                break;
              }
            }
          }
          if (!gotResponse) {
            requestPromises.push({status: 404, statusText: 'Not Found Error'});
          }
        } else if (request.url.search(/file:\/\//i) !== -1) {
          // TODO M - Perform 'fetch' query/write on filesystem
        } else {
          requestPromises.push(Reflect.apply(target, thisArg, [request]));
        }
      }
      if (requestPromises.length === 1) {
        return requestPromises[0];
      }
      // Result: <Array<{status, value}>>
      return Promise.allSettled(requestPromises);
    },
  };
};

async function fetchCallback({requestId, argumentsList}) {
  try {
    let fetchResponse = await fetch(...argumentsList);
    // HACK - Nodejs-Mobile is weird so you need to call 'val.constructor.constructor.name' to identify an AsyncGeneratorFunctionFunction instead of just 'val.constructor.name'
    // TODO M - Refactor out function dedicated to determining if body warrants a nodejs-bridge listener
    if (Array.isArray(fetchResponse)) {
      // Iterating without posting to channel to ensure React Native registers listeners beforehand
      let initialResponseArray = [];
      for (var i = 0; i < fetchResponse.length; i++) {
        if (
          fetchResponse[i].status === 'fulfilled' &&
          fetchResponse[i].value &&
          fetchResponse[i].value.body &&
          (typeof fetchResponse[i].value.body === 'function' ||
            (typeof fetchResponse[i].value.body === 'object' &&
              fetchResponse[i].value.body.constructor &&
              fetchResponse[i].value.body.constructor.constructor &&
              fetchResponse[i].value.body.constructor.constructor.name ===
                'AsyncGeneratorFunction'))
        ) {
          // Add bodyId
          const randomInt = Math.floor(Math.random() * Math.floor(100000));
          const bodyId = 'fetch-body*' + new Date().getTime() + '*' + randomInt;
          fetchResponse[i].value.bodyId = bodyId;
          const {body, ...bodylessResponse} = fetchResponse[i].value;
          initialResponseArray.push({...bodylessResponse});
        }
      }
      // Send array to React Native
      rn_bridge.channel.post(requestId, initialResponseArray);
      // Add body listeners
      for (var i = 0; i < fetchResponse.length; i++) {
        const fetchResult = fetchResponse[i];
        if (fetchResult.value && fetchResult.value.bodyId) {
          const bodyId = fetchResult.value.bodyId;
          try {
            if (typeof fetchResult.value.body === 'function') {
              // Handle like a promise
              const result = await fetchResult.value.body;
              console.log('[NODE] Body Message:', bodyId, result);
              rn_bridge.channel.post(bodyId, {result});
            } else {
              // Handle like an iterable
              var firstResponse = false;
              var bridgeIteration = -1;
              for await (const result of fetchResult.value.body) {
                let thisIteration = bridgeIteration + 1;
                bridgeIteration += 1;
                if (!firstResponse) {
                  firstResponse = true;
                  console.log('[NODE] Body Message:', bodyId, result);
                  rn_bridge.channel.post(bodyId, {
                    iteration: thisIteration,
                    result,
                  });
                } else {
                  // Timeout is a workaround to hopefully put less stress on the RN-Nodejs bridge
                  setTimeout(() => {
                    console.log('[NODE] Body Message:', bodyId, result);
                    rn_bridge.channel.post(bodyId, {
                      iteration: thisIteration,
                      result,
                    });
                  }, 50);
                }
              }
              console.log('[NODE] Body Message:', bodyId);
              rn_bridge.channel.post(bodyId, {end: bridgeIteration});
            }
          } catch (bodyError) {
            console.log('[NODE] Body Error:', bodyId, bodyError);
            const error = formatError(bodyError);
            rn_bridge.channel.post(bodyId, {error});
          }
        }
      }
    } else {
      // Single response
      if (
        fetchResponse.body &&
        (typeof fetchResponse.body === 'function' ||
          (typeof fetchResponse.body === 'object' &&
            fetchResponse.body.constructor &&
            fetchResponse.body.constructor.constructor &&
            fetchResponse.body.constructor.constructor.name ===
              'AsyncGeneratorFunction'))
      ) {
        // Send back with bodyId
        const randomInt = Math.floor(Math.random() * Math.floor(100000));
        const bodyId = 'fetch-body*' + new Date().getTime() + '*' + randomInt;
        fetchResponse.bodyId = bodyId;
        const {body, ...bodylessResponse} = fetchResponse;
        console.log('[NODE] Initial fetch response for id:', requestId, {
          ...bodylessResponse,
        });
        rn_bridge.channel.post(requestId, {...bodylessResponse});
        try {
          if (typeof fetchResponse.body === 'function') {
            // Handle like a promise
            const result = await fetchResponse.body;
            rn_bridge.channel.post(bodyId, {result});
          } else {
            // Handle like an iterable
            var firstResponse = false;
            var bridgeIteration = -1;
            for await (const result of fetchResponse.body) {
              let thisIteration = bridgeIteration + 1;
              bridgeIteration += 1;
              if (!firstResponse) {
                firstResponse = true;
                console.log(
                  '[NODE] Fetch body response iteration:',
                  {
                    iteration: thisIteration,
                    result,
                  },
                  'for bodyId:',
                  bodyId,
                );
                rn_bridge.channel.post(bodyId, {
                  iteration: thisIteration,
                  result,
                });
              } else {
                console.log(
                  '[NODE] Fetch body response iteration:',
                  {
                    iteration: thisIteration,
                    result,
                  },
                  'for bodyId:',
                  bodyId,
                );
                rn_bridge.channel.post(bodyId, {
                  iteration: thisIteration,
                  result,
                });
              }
            }
            rn_bridge.channel.post(bodyId, {end: bridgeIteration});
          }
        } catch (bodyError) {
          console.log(
            '[NODE] Fetch body error:',
            bodyError,
            'for bodyId:',
            bodyId,
          );
          const error = formatError(bodyError);
          rn_bridge.channel.post(bodyId, {error});
        }
      } else {
        // Send back as is
        rn_bridge.channel.post(requestId, fetchResponse);
      }
    }
  } catch (fetchError) {
    const error = formatError(fetchError);
    rn_bridge.channel.post(requestId, {error});
  }
}

module.exports = {
  functionFromString,
  fetchHandler,
  fetchCallback,
};
