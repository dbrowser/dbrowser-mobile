const rn_bridge = require('rn-bridge');
const jsqr = require('jsqr');
var image = require('get-image-data');

rn_bridge.channel.on('decode-qr', async msg => {
  // NOTE - params should always at least have a unique ID for a callback to RN
  let {path, requestId} = msg;
  image(path, function(err, info) {
    if (err || info === null || info === undefined) {
      console.log('[NODE] Image info error:', err);
      rn_bridge.channel.post(requestId, {error: err});
    }
    if (info && info !== null && info !== undefined) {
      var data = info.data;
      var height = info.height;
      var width = info.width;
      const code = jsqr(data, width, height);
      if (code === null || code === undefined || !code.data) {
        console.log('[NODE] No QR Code');
        rn_bridge.channel.post(requestId, {
          error: 'Could not find a QR code in this image',
        });
      } else {
        rn_bridge.channel.post(requestId, {result: code.data});
      }
    }
  });
});
