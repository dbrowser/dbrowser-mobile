const markdown = require('./markdown.js');

const mdNoHTML = markdown({
  allowHTML: false,
  useHeadingIds: false,
  useHeadingAnchors: false,
  hrefMassager: undefined,
  highlight: undefined,
});
const mdWithHTML = markdown({
  allowHTML: true,
  useHeadingIds: false,
  useHeadingAnchors: false,
  hrefMassager: undefined,
  highlight: undefined,
});

module.exports = {
  toHTML(str, {allowHTML} = {}) {
    if (allowHTML) {
      return mdWithHTML.render(str);
    }
    return mdNoHTML.render(str);
  },
};
