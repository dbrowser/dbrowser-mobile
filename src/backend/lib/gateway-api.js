// ARCHIVED - Gateway's Node API Prior to major refactor
//          - Just used as a reference until all functionality is translated

var rn_bridge = require('rn-bridge');
const {join} = require('path');
const {emptyDirSync, ensureDir} = require('fs-extra');
const {mkdirSync, existsSync} = require('fs');
const Flatted = require('flatted/cjs');
const {fetchCallback, parseURLCallback} = require('./generators.js');

const datPermStorageLocation = join(global.dataDir, '/.local/share/dat-nodejs');
const datTempStorageLocation = join(global.tmpDir, '/.local/share/dat-nodejs');
var isDatFsSetup;

function isClass(func) {
  return (
    typeof func === 'function' &&
    /^class\s/.test(Function.prototype.toString.call(func))
  );
}

var zombified = {};

const zombify = (mod, ctx, opts = {}) => {
  if (typeof ctx === 'object') {
    opts = ctx;
    ctx = 'start';
  }
  const parents = opts.parents || [];
  const split = opts.split || false;
  const moduleType = typeof mod;
  const modName = opts.name || mod.name || mod.constructor.name;
  var result;
  if (mod === null) {
    result = 'null';
  } else if (mod === undefined) {
    result = 'undefined';
  } else if (opts.depth && parents.length >= opts.depth) {
    console.log('[DAT] Depth met!', parents.length, opts.depth);
    result = Flatted.stringify(mod);
  } else if (
    moduleType === 'object' &&
    mod !== null &&
    mod !== undefined &&
    !Buffer.isBuffer(mod)
  ) {
    // Object
    var startStr = '{\n';
    if (ctx === 'window') {
      if (split) {
        startStr = '';
      } else {
        startStr = `window.${modName} = {\n`;
        zombified[modName] = mod;
      }
    }
    const entries = Object.entries(mod);
    for (let [key, value] of entries) {
      let prefix = `  ${key}: `;
      let suffix = ',\n';
      if (ctx === 'window' && split) {
        prefix = `window.${key} = `;
        suffix = '\n';
        zombified[key] = value;
      }
      const nextParents =
        ctx === 'window' && split ? parents : parents.concat([modName]);
      startStr =
        startStr +
        prefix +
        zombify(value, 'obj', {
          name: key,
          parents: nextParents,
          depth: opts.depth,
        }) +
        suffix;
    }
    const endStr = ctx === 'window' ? (split ? '\n' : '}\n') : '  }';
    result = startStr + endStr;
  } else if (isClass(mod)) {
    // Class
    const moduleArray = parents.concat([modName]);
    var startStr = `class ${mod.name} {
  constructor (...params) {
    if (!window.nodeCallbacks) {
      window.nodeCallbacks = {};
    }
    Promise.resolve().then(async () => {
      await new Promise((resolve, reject) => {
        const randomInt = Math.floor(Math.random() * Math.floor(100000));
        const requestId = '()' + '*' + new Date().getTime() + '*' + randomInt;
        const parameters = [..params];
        const zombieParams = parameters.map(param => {
          if (typeof param === 'function') {
            return '[FUNCTION]';
          } else {
            return param;
          }
        });
        window.ReactNativeWebView.postMessage(JSON.stringify({module: ${JSON.stringify(
          moduleArray,
        )}, requestId, uniqueNodeId: this.uniqueNodeId, params: zombieParams}));
        (function waitForResults(){
          const nodeResults = nodeCallbacks[requestId];
          if (nodeResults) {
            if (nodeResults.result && typeof nodeResults.result === 'string' && nodeResults.result.indexOf("ERROR:") === 0) {
              window.ReactNativeWebView.postMessage(nodeResults.result);
              return reject(nodeResults.result);
            }
            if (nodeResults.callbacks) {
              const callbackEntries = Object.entries(nodeResults.callbacks);
              for (let [key, value] of callbackEntries) {
                let index = typeof key !== 'number' ? Number(key) : key;
                parameters[index](...value);
              }
            }
            if (typeof nodeResults.result === 'object') {
              for (let [key, value] of Object.entries(nodeResults.result)) {
                this[key] = value;
              }
            }
            return resolve(nodeResults.result);
          }
          setTimeout(waitForResults, 30);
        })();
      });
    });
  }\n`;
    if (ctx === 'window') {
      startStr = `window.${modName} = ` + startStr;
      zombified[modName] = mod;
    }
    for (let propKey of Object.getOwnPropertyNames(mod)) {
      const prop = mod[propKey];
      const request = Buffer.isBuffer(prop) ? propKey + '[BUFFER]' : propKey;
      let declaration = `static async ${propKey} (...params) {
    return nodeRequest({module: ${JSON.stringify(
      moduleArray,
    )}, request: '${request}', uniqueNodeId: this.uniqueNodeId, params: [...params]});
}`;
      if (isClass(prop) || typeof prop !== 'function') {
        declaration = `static ${propKey} = ${zombify(prop, 'class', {
          name: propKey,
          parents: moduleArray,
          depth: opts.depth,
        })}`;
      }
      startStr = startStr + declaration + '\n';
    }
    const endStr = ctx === 'window' ? '}\n' : '}';
    result = startStr + endStr;
  } else if (moduleType === 'function' || Buffer.isBuffer(mod)) {
    // Function
    let moduleArray = parents.length === 0 ? [modName] : parents;
    let request = parents.length === 0 ? '()' : modName;
    if (Buffer.isBuffer(mod)) {
      request = request + '[BUFFER]';
    }
    let string = `async function ${mod.name} (...params) {
    return nodeRequest({module: ${JSON.stringify(
      moduleArray,
    )}, request: '${request}', uniqueNodeId: this.uniqueNodeId, params: [...params]});
}`;
    if (ctx === 'window') {
      string = `window.${modName} = ` + string + '\n';
      zombified[modName] = mod;
    }
    result = string;
  } else if (Array.isArray(mod)) {
    // Array
    var startStr = '[\n';
    if (ctx === 'window') {
      startStr = `window.${modName} = [\n`;
      zombified[modName] = mod;
    }
    for (var i = 0; i < mod.length; i++) {
      const element = mod[i];
      const nextParents = parents.concat([modName]);
      startStr =
        startStr +
        zombify(element, 'array', {
          name: i,
          parents: nextParents,
          depth: opts.depth,
        }) +
        ',\n';
    }
    const endStr = ctx === 'window' ? ']\n' : ']';
    result = startStr + endStr;
  } else {
    result = JSON.stringify(mod);
    if (ctx === 'window') {
      result = `window.${modName} = ` + result + '\n';
      zombified[modName] = mod;
    }
  }
  if (ctx === 'window') {
    rn_bridge.channel.post('webview-injection', {
      type: 'SET_WEBVIEW_WINDOW_MODULE',
      module: 'Dat',
      script: result,
    });
  }
  return result;
};

let startedSetup = false;

const setupDat = async () => {
  if (!startedSetup) {
    startedSetup = true;
    const permStorageExists = existsSync(datPermStorageLocation);
    if (!permStorageExists) {
      console.log('[DAT] Creating Permanent Storage Location!');
      mkdirSync(datPermStorageLocation, {recursive: true});
      rn_bridge.channel.post('set-dat-fs', {
        type: 'SET_DAT_PERM_PATH',
        path: datPermStorageLocation,
      });
    }
    const tempStorageExists = existsSync(datTempStorageLocation);
    if (!tempStorageExists) {
      console.log('[DAT] Creating Temporary Storage Location!');
      mkdirSync(datTempStorageLocation, {recursive: true});
      rn_bridge.channel.post('set-dat-fs', {
        type: 'SET_DAT_TEMP_PATH',
        path: datTempStorageLocation,
      });
    } else if (!isDatFsSetup) {
      emptyDirSync(datTempStorageLocation);
    }
    // if (!DatArchive) {
    //   const LegacyDatArchive = LegacyDatSDKPromise(datOptions);
    //   DatArchive = LegacyDatArchive.DatArchive;
    //   destroyDatArchive = LegacyDatArchive.destroy;
    // }
    isDatFsSetup = true;
    if (Dat === undefined) {
      console.log('[DAT] Dat is not setup yet...');
      Dat = await DatSDK(datOptions);
      console.log('[DAT] Dat is setup!');
      const zombie = zombify(Dat, 'window', {
        name: Object.keys({Dat})[0],
        split: true,
      });
      console.log('[DAT] Zombified:', zombie);
    }
  }
};

// How long to wait to get peers / sync with them
const READY_DELAY = 3000;
const BASE_32_KEY_LENGTH = 52;

const DNS_PROVIDER_OPTIONS = [
  ['cloudflare-dns.com', 443, '/dns-query'],
  ['dns.quad9.net', 5053, '/dns-query'],
  ['doh.opendns.com', 443, 'dns-query'],
];
let dnsProvider =
  DNS_PROVIDER_OPTIONS[Math.floor(Math.random() * DNS_PROVIDER_OPTIONS.length)];
console.log('[DAT] Provider for DNS lookups:', dnsProvider);

const datOptions = {
  storageOpts: {
    application: 'dat',
    storageLocation: datPermStorageLocation,
  },
  dnsOpts: {
    dnsHost: dnsProvider[0],
    dnsPort: dnsProvider[1],
    dnsPath: dnsProvider[2],
  },
};

// const LegacyDatSDK = require('dat-sdk');
// const LegacyDatSDKPromise = require('dat-sdk/promise');
const DatSDK = require('dat-sdk');
let DatArchive;
let destroyDatArchive;
// const DatLegacy = LegacyDatSDK(datOptions);
var Dat;

const pda1 = require('pauls-dat-api');
const pda2 = require('pauls-dat-api2');

const {hasDirectoryName, exportArchiveToFilesystem} = require('./dat-common');
const {formatError} = require('./common');
// const parseURL = require('url-parse');
const hexTo32 = require('hex-to-32');

const dats = new Map();

// async function getURLData(url, isLegacy) {
//   let key = null;
//   let version = null;
//   let protocol = null;
//   let pathname = null;
//
//   if (url) {
//     if (url.indexOf('://') === -1) {
//       url = isLegacy ? `dat://${url}` : `hyper://${url}`;
//     }
//     const parsed = parseURL(url);
//     let hostname = null;
//     protocol =
//       parsed.protocol.indexOf('undefined') === 0 ? 'hyper:' : parsed.protocol;
//     pathname = parsed.pathname;
//     if (parsed.hostname.indexOf('.') === -1) {
//       const hostnameParts = parsed.hostname.split('+');
//       hostname = hostnameParts[0];
//       key = hostname;
//       version = hostnameParts[1] || null;
//       if (version === '') {
//         version = null;
//       }
//       if (typeof version === 'string') {
//         version = Number(version);
//       }
//     } else {
//       const hostnameParts = parsed.hostname.split('.');
//       const subdomain = hostnameParts[0];
//       if (subdomain.length === BASE_32_KEY_LENGTH) {
//         hostname = hexTo32.decode(subdomain);
//       } else {
//         hostname = parsed.hostname;
//       }
//       const SDK = isLegacy ? DatArchive : Dat;
//       try {
//         key = await SDK.resolveName(`dat://${hostname}`);
//       } catch (keyError) {
//         throw keyError;
//       }
//     }
//   }
//
//   return {
//     key,
//     version,
//     protocol,
//     pathname: pathname || '/',
//   };
// }

function waitReady(archive) {
  return new Promise((resolve, reject) => {
    archive.ready(err => {
      if (err) {
        console.log('[DAT] waitReady Rejection', err);
        reject(err);
      } else {
        resolve(archive);
      }
    });
  });
}

async function reallyReady(archive) {
  if (archive.writable) {
    return;
  }

  const files = new Promise((resolve, reject) => {
    archive.readdir('/', (err, files) => (err ? resolve([]) : resolve(files)));
  });

  if (files.length) {
    return;
  }

  return new Promise((resolve, reject) => {
    function cb(err, result) {
      // Ignore errors saying we're up to date
      console.log('[DAT] Really Ready Callback. Err:', err, ' Result:', result);
      if (err && err.message !== 'No update available from peers') {
        reject(err);
      } else {
        resolve(result);
      }
    }
    if (archive.metadata.peers.length) {
      archive.metadata.update({ifAvailable: true}, cb);
    } else {
      const timeout = setTimeout(cb, READY_DELAY);
      archive.metadata.once('peer-add', () => {
        console.log('[DAT] Really Ready, Peer added!');
        clearTimeout(timeout);
        archive.metadata.update({ifAvailable: true}, cb);
      });
    }
  });
}

// NOTE - It's currently pretty rough having legacy support, so it's not supported right now
async function tryLegacyOrEnd(params) {
  const {archive, legacy, e} = params;
  // if (!legacy) {
  //   console.log('[DAT] Might be Legacy Dat. Closing...');
  //   // Assuming this archive was just created because it doesn't exist
  //   // Trying to load through DatArchive
  //   if (archive.close) {
  //     archive.close(async () => {
  //       console.log(
  //         '[DAT] Hyperdrive closed! Now trying legacy... (close function)',
  //       );
  //       const newParams = Object.assign(params, {legacy: true});
  //       try {
  //         const results = await getArchive(newParams);
  //         return results;
  //       } catch (getArchiveError) {
  //         console.log(
  //           '[DAT] Legacy dat also failed. Probably does not exist or cannot be accessed. 1',
  //         );
  //         return {error: getArchiveError};
  //       }
  //     });
  //   } else {
  //     console.log(
  //       '[DAT] Hyperdrive closed! Now trying legacy... (no close function)',
  //     );
  //     const newParams = Object.assign(params, {legacy: true});
  //     try {
  //       const results = await getArchive(newParams);
  //       return results;
  //     } catch (getArchiveError) {
  //       console.log(
  //         '[DAT] Legacy dat also failed. Probably does not exist or cannot be accessed. 3',
  //       );
  //       return {error: getArchiveError};
  //     }
  //   }
  // } else {
  if (archive.close) {
    console.log(
      '[DAT] Legacy dat also failed. Probably does not exist or cannot be accessed. (archive close)',
    );
    archive.close(() => {
      return {error: e || 'Archive does not appear to exist.'};
    });
  } else {
    console.log(
      '[DAT] Legacy dat also failed. Probably does not exist or cannot be accessed. (no close)',
    );
    return {error: e || 'Archive does not appear to exist.'};
  }
  // }
}

const getArchive = async params => {
  const {baseUrl, key, urlVersion, legacy} = params;
  const pda = legacy ? pda1 : pda2;
  let archive = dats.get(baseUrl);
  let version = urlVersion;
  let archiveVersion;
  let archiveWritable;
  let latestArchiveVersion;
  let isLegacy = legacy;
  let error;
  if (!archive) {
    console.log('[DAT] Archive is undefined. Trying to load it...');
    try {
      // const dat = legacy ? DatArchive : Dat;
      archive = legacy
        ? await DatArchive.load(`dat://${key}`)
        : await Dat.Hyperdrive(key);
      console.log('[DAT] Loaded archive! Legacy?', legacy);
      if (!legacy) {
        console.log('[DAT] Checking if archive is ready...');
        await waitReady(archive);
        console.log('[DAT] Archive is ready!');
        await reallyReady(archive);
      }
      archiveVersion = archive._checkout
        ? archive._checkout.version
        : archive.version;
      archiveWritable = archive._archive
        ? archive._archive.writable
        : archive.writable;
      console.log(
        '[DAT] Archive is REALLY ready!',
        archiveVersion,
        archiveWritable,
      );
      const test = legacy
        ? await archive.readdir('/')
        : await pda.readdir(archive, '/');
      console.log('[DAT] TEST!!!', test);
      if (!test || test.length === 0) {
        const legacyAttempt = await tryLegacyOrEnd(params);
        return legacyAttempt;
      }
      dats.set(baseUrl, archive);
      latestArchiveVersion = archiveVersion;
      console.log('[DAT] Archive starting at version:', archiveVersion);
      if (version && version < archiveVersion) {
        console.log('[DAT] Checking out version:', version);
        archive = await archive.checkout(version);
        archiveVersion = version;
        // dats.set(versionUrl, archive);
      } else {
        console.log('[DAT] No version to checkout!');
        version = null;
      }
    } catch (e) {
      console.log('[DAT] Caught an error:', e);
      const newParams = Object.assign(params, {e});
      const legacyAttempt = await tryLegacyOrEnd(newParams);
      return legacyAttempt;
    }
  } else if (version) {
    if (!legacy && archive._checkout) {
      isLegacy = true;
    }
    archiveVersion = archive._checkout
      ? archive._checkout.version
      : archive.version;
    archiveWritable = archive._archive
      ? archive._archive.writable
      : archive.writable;
    if (version < archiveVersion) {
      latestArchiveVersion = archiveVersion;
      archive = await archive.checkout(version);
      archiveVersion = version;
      // dats.set(versionUrl, archive);
    }
  } else {
    if (!legacy && archive._checkout) {
      isLegacy = true;
    }
    archiveVersion = archive._checkout
      ? archive._checkout.version
      : archive.version;
    archiveWritable = archive._archive
      ? archive._archive.writable
      : archive.writable;
    latestArchiveVersion = archiveVersion;
    version = null;
  }
  return {
    archive,
    version,
    archiveVersion,
    archiveWritable,
    latestArchiveVersion,
    isLegacy,
    error,
  };
};

const requestDat = async params => {
  let {
    url,
    tabIndex,
    viewType,
    fsPath: prevFsPath,
    forceLoad,
    urlData,
    legacy,
  } = params;
  let pushHistory = true;
  let {key, version: _version, protocol, pathname} = urlData;
  if (!key) {
    console.log('[DAT] ERROR: Archive Load Error: No Key 0');
    rn_bridge.channel.post('browser-error', {
      url,
      tabIndex,
      error: formatError('Could not detect dat key.'),
    });
    return;
  }
  if (legacy) {
    protocol = 'dat:';
  }
  if (prevFsPath) {
    const pathExists = existsSync(prevFsPath);
    console.log('[DAT] Request path exists:', prevFsPath);
    if (pathExists && _version) {
      console.log(
        '[DAT] The version was specified. No need to proceed:',
        prevFsPath,
      );
      return;
    } else if (pathExists && !forceLoad) {
      pushHistory = false;
    }
  } else {
    console.log('[DAT] Fs path does not exist.');
  }
  console.log('[DAT] URL Data:', urlData);
  console.log('[DAT] Attempting to load Archive for:', url);
  const versionStr = _version ? `+${_version}` : '';
  const baseUrl = protocol + '//' + key;
  const versionUrl = baseUrl + versionStr;
  let datData;
  try {
    datData = await getArchive({
      baseUrl,
      key,
      urlVersion: _version,
      legacy,
    });
  } catch (getArchiveError) {
    console.log('[DAT] ERROR: getArchiveError 1', getArchiveError);
    rn_bridge.channel.post('browser-error', {
      url,
      tabIndex,
      error: formatError(getArchiveError),
    });
    return;
  }
  let {
    archive,
    version,
    archiveVersion,
    archiveWritable,
    latestArchiveVersion,
    isLegacy,
    error,
  } = datData;
  if (error) {
    console.log('[DAT] ERROR: error 2', error);
    rn_bridge.channel.post('browser-error', {
      url,
      tabIndex,
      error: formatError(error),
    });
    return;
  }
  if (isLegacy) {
    legacy = true;
    protocol = 'dat:';
  }
  const pda = legacy ? pda1 : pda2;
  if (prevFsPath) {
    console.log('[DAT] Previous FS Path exists!', prevFsPath);
    const fsArray = prevFsPath.split('/');
    console.log('[DAT] Previous FS Path Array:', fsArray);
    const prevFsVersion = Number(fsArray[fsArray.indexOf(key) + 1]);
    console.log('[DAT] Previous FS Version:', prevFsVersion);
    if (prevFsVersion === archiveVersion && !forceLoad) {
      console.log('[DAT] FS for this version exists. No need to proceed!');
      return;
    }
  }
  if (pushHistory) {
    rn_bridge.channel.post('dat-navigation-progress', {
      url,
      tabIndex,
      progress: 0.0,
    });
  }
  if (key && typeof key !== 'string') {
    key = key.toString('hex');
  }
  let discoveryKey = archive._archive
    ? archive._archive.discoveryKey
    : archive.discoveryKey;
  if (discoveryKey && typeof discoveryKey !== 'string') {
    discoveryKey = discoveryKey.toString('hex');
  }
  let info = {
    key,
    discoveryKey,
    writable: archiveWritable,
    legacy,
  };
  rn_bridge.channel.post('set-dat-info', {
    key,
    info,
  });
  console.log('[DAT] Loaded Archive Info!', info);
  rn_bridge.channel.post('set-dat-version', {
    key,
    currentVersion: latestArchiveVersion,
    seenVersion: version || archiveVersion,
  });
  console.log(
    '[DAT] Archive Version:',
    archiveVersion,
    'Latest Version:',
    latestArchiveVersion,
  );
  let manifest = {};
  try {
    manifest = legacy
      ? await archive.getManifest()
      : await pda.readManifest(archive);
    rn_bridge.channel.post('set-dat-manifest', {
      key,
      manifest,
      version: version || archiveVersion,
    });
  } catch (e) {
    console.log('[DAT] Manifest Read Error:', e);
  }
  let results = {};
  if (pushHistory) {
    rn_bridge.channel.post('dat-navigation-progress', {
      url,
      tabIndex,
      progress: 0.5,
    });
  }
  const webRoot = manifest.web_root || '';
  const webPath = webRoot + pathname;
  let path = viewType === 'site' ? webPath : pathname;
  if (viewType === 'site') {
    let test;
    try {
      test = legacy
        ? await archive.stat(webPath)
        : await pda.stat(archive, webPath);
    } catch (e) {
      // webPath doesn't exist. Just go with the pathname
    }
    if (test) {
      path = webPath;
    }
  }
  if (viewType === 'sourceFromSite' && pathname.endsWith('.html')) {
    path = path.substring(0, path.lastIndexOf('/') + 1);
  }

  // NOTE - Determining if a folder is based on if stats.offset === 0
  const tempBaseDirectory = `${datTempStorageLocation}/${key}/${archiveVersion}`;

  try {
    console.log('[DAT] Checking stat of path:', path);
    const pathStats = legacy
      ? await archive.stat(path)
      : await pda.stat(archive, path);
    console.log('[DAT] pathStats:', pathStats);
    if ((legacy && !hasDirectoryName(path, pathStats)) || pathStats.isFile()) {
      results.url = versionUrl + path;
      console.log('[DAT] Path is to a file!');
      if (path.endsWith('.html')) {
        rn_bridge.channel.post('set-dat-info', {
          key,
          info: {
            site: true,
          },
        });
      }
      if (path.endsWith('.html') && viewType === 'site') {
        console.log('[DAT] Path is to an html file for a site!');
        results.html = tempBaseDirectory + path;
      } else {
        const readOpts = {encoding: 'utf8'};
        const data = legacy
          ? await archive.readFile(path, readOpts)
          : await pda.readFile(archive, path, readOpts);
        results.contents = {name: path, stat: pathStats, data};
      }
    } else if (
      (legacy && hasDirectoryName(path, pathStats)) ||
      pathStats.isDirectory()
    ) {
      console.log('[DAT] Path is to a directory!');
      const readOpts = legacy ? {stat: true} : {includeStats: true};
      let pathContents = legacy
        ? await archive.readdir(path, readOpts)
        : await pda.readdir(archive, path, readOpts);
      console.log('[DAT] Got Directory Contents!');
      const htmlContents = legacy
        ? pathContents.filter(obj => obj.endsWith('.html'))
        : pathContents.filter(obj => obj.name.endsWith('.html'));
      console.log('[DAT] Html Contents:', htmlContents);
      if (htmlContents.length > 0) {
        rn_bridge.channel.post('set-dat-info', {
          key,
          info: {
            site: true,
          },
        });
      }
      if (viewType === 'site' && htmlContents.length > 0) {
        const folderName = path.split('/').reverse()[0];
        let filePath = path;
        if (
          (legacy && htmlContents.find(x => x === 'index.html')) ||
          (!legacy && htmlContents.find(x => x.name === 'index.html'))
        ) {
          filePath = join(path, '/index.html');
        } else if (
          legacy &&
          htmlContents.find(x => x === folderName + '.html') &&
          (!legacy && htmlContents.find(x => x.name === folderName + '.html'))
        ) {
          filePath = join(path, '/' + folderName + '.html');
        } else {
          const fileName = legacy
            ? '/' + htmlContents[0]
            : '/' + htmlContents[0].name;
          filePath = join(path, fileName);
          path = filePath;
        }
        results.url = versionUrl + path;
        results.html = tempBaseDirectory + filePath;
        console.log('[DAT] Found site html:', results.html);
      } else {
        results.url = versionUrl + path;
        results.contents = pathContents;
      }
    }

    if (pushHistory) {
      rn_bridge.channel.post('dat-navigation-progress', {
        url,
        tabIndex,
        progress: 0.75,
      });
    }

    if (results.html) {
      console.log('[DAT] This is a website.');
      const htmlExists = existsSync(results.html);
      if (!htmlExists) {
        console.log('[DAT] Writing to filesystem...', results.html);
        const fsStats = await exportArchiveToFilesystem({
          srcArchive: archive,
          dstPath: tempBaseDirectory,
          skipUndownloadedFiles: false,
          overwriteExisting: true,
          transformRelativePaths: true,
          // srcPath: path,
          // walkStart: true,
        });
        console.log('[DAT] Wrote entire archive to filesystem!', fsStats);
      } else {
        console.log('[DAT] Site already written to filesystem!', results.html);
      }
      if (pushHistory) {
        console.log('[DAT] Returning to React Native! 1');
        rn_bridge.channel.post('push-dat', {tabIndex, source: results});
        rn_bridge.channel.post('dat-navigation-progress', {
          url,
          tabIndex,
          progress: 1.0,
        });
      }
    } else {
      if (pushHistory) {
        console.log('[DAT] Returning to React Native! 2');
        rn_bridge.channel.post('push-dat', {tabIndex, source: results});
        rn_bridge.channel.post('dat-navigation-progress', {
          url,
          tabIndex,
          progress: 1.0,
        });
      }
    }
    // NOTE - Don't need to worry about missing a new version because the banner will tell the user anyway
  } catch (e) {
    console.log('[DAT] ERROR: e 3', e);
    rn_bridge.channel.post('browser-error', {url, tabIndex, error: formatError(e)});
  }
};

rn_bridge.channel.on('submit-dat', async message => {
  let msg = message;
  console.log('[DAT] Submit Request:', msg);
  try {
    await setupDat();
  } catch (setupError) {
    startedSetup = false;
    console.log('[DAT] ERROR: setupError 4', setupError);
    rn_bridge.channel.post('browser-error', {
      tabIndex: msg.tabIndex,
      url: msg.url,
      error: formatError(setupError),
    });
    return;
  }
  let urlData;
  try {
    urlData = await parseURL(msg.url);
  } catch (urlDataError) {
    console.log('[DAT] ERROR: urlDataError 5', urlDataError);
    rn_bridge.channel.post('browser-error', {
      tabIndex: msg.tabIndex,
      url: msg.url,
      error: formatError(urlDataError),
    });
    return;
  }
  if (urlData.pathname === '/' && !msg.url.endsWith('/')) {
    msg.url = msg.url + '/';
  }
  const params = Object.assign(msg, {urlData});
  if (
    urlData.protocol === 'dat:' ||
    urlData.protocol === 'hyper:' ||
    urlData.protocol === null
  ) {
    requestDat(params);
  } else {
    // TODO M - Custom protocols
    console.log('[DAT] This protocol is not supported yet', urlData.protocol);
  }
});

const shareFileContent = async ({requestId, requestUrl, urlData}) => {
  let {key, version: _version, protocol, pathname} = urlData;
  const baseUrl = protocol + '//' + key;
  let datData;
  try {
    datData = await getArchive({
      baseUrl,
      key,
      urlVersion: _version,
    });
  } catch (getArchiveError) {
    console.log('[DAT] ERROR: getArchiveError 6', getArchiveError);
    rn_bridge.channel.post('browser-error', {
      url: requestUrl,
      error: formatError(getArchiveError),
    });
    return;
  }
  let {archive, archiveVersion, error} = datData;
  if (error) {
    console.log('[DAT] ERROR: error 7', error);
    rn_bridge.channel.post('browser-error', {
      url: requestUrl,
      error: formatError(error),
    });
    return;
  }
  const tempBaseDirectory = `${datTempStorageLocation}/${key}/${archiveVersion}`;
  const filePath = join(tempBaseDirectory, pathname);
  const writePath = filePath.substring(0, filePath.lastIndexOf('/'));
  console.log('Ensuring path...', writePath);
  try {
    await ensureDir(writePath);
  } catch (ensureDirError) {
    console.log('[DAT] ERROR: ensureDirError 8', ensureDirError);
    rn_bridge.channel.post('browser-error', {
      url: requestUrl,
      error: formatError(ensureDirError),
    });
  }
  console.log('Path ensured!', writePath);
  try {
    const fsStats = await exportArchiveToFilesystem({
      srcArchive: archive,
      dstPath: filePath,
      skipUndownloadedFiles: false,
      overwriteExisting: true,
      srcPath: pathname,
    });
    console.log('[DAT] Wrote entire archive to filesystem!', fsStats);
    rn_bridge.channel.post(requestId, {
      filePath,
    });
  } catch (exportError) {
    console.log('[DAT] ERROR: exportError 9', exportError);
    rn_bridge.channel.post('browser-error', {
      url: requestUrl,
      error: formatError(exportError),
    });
  }
};

rn_bridge.channel.on(
  'share-dat-content',
  async ({requestId, url: requestUrl}) => {
    let url = requestUrl;
    console.log('[DAT] Sharing:', requestId, url);
    try {
      await setupDat();
    } catch (setupError) {
      startedSetup = false;
      console.log('[DAT] ERROR: setupError 10', setupError);
      rn_bridge.channel.post('browser-error', {
        url,
        error: formatError(setupError),
      });
      return;
    }
    let urlData;
    try {
      urlData = await parseURL(url);
    } catch (urlDataError) {
      console.log('[DAT] ERROR: urlDataError 11', urlDataError);
      rn_bridge.channel.post('browser-error', {
        url,
        error: formatError(urlDataError),
      });
      return;
    }
    if (urlData.pathname === '/' && !url.endsWith('/')) {
      url = url + '/';
    }
    const params = Object.assign({requestId, requestUrl: url}, {urlData});
    if (
      urlData.protocol === 'dat:' ||
      urlData.protocol === 'hyper:' ||
      urlData.protocol === null
    ) {
      shareFileContent(params);
    } else {
      // TODO M - Custom protocols
      console.log('[DAT] This protocol is not supported yet', urlData.protocol);
    }
  },
);

// ----------- Web-RN-Node Bridge -----------

rn_bridge.channel.on('parseURL', ({requestId, url}) => {
  // NOTE - params should always at least have a unique ID for a callback to RN
  parseURLCallback({requestId, url});
});

console.log('[NODE] Fetch listener?');

rn_bridge.channel.on('fetch', ({requestId, url, opts}) => {
  console.log('[NODE] Starting fetch from bridge...', requestId, url, opts);
  // NOTE - params should always at least have a unique ID for a callback to RN
  fetchCallback({requestId, url, opts});
});

console.log('[NODE] Fetch listener!');

const bridgeRequestError = (error, requestId) => {
  console.log('[DAT] Request Error:', error, 'For Request:', requestId);
  rn_bridge.channel.post(requestId, `ERROR: ${error}`);
};

const promisify = (func, params) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await func.apply(null, params);
      return resolve(result);
    } catch (e) {
      return reject(e);
    }
  });
};

// TODO - Solution for streams, emmitters, updating values
rn_bridge.channel.on(
  'web-api-bridge',
  async ({module: nodeModule, requestId, uniqueNodeId, params}) => {
    console.log(
      '[DAT] Node received bridge request:',
      nodeModule,
      requestId,
      uniqueNodeId,
      params,
    );
    let zombie = zombified;
    for (var i = 0; i < nodeModule.length; i++) {
      let submod = nodeModule[i];
      zombie = zombie[submod];
      if (!zombie) {
        bridgeRequestError(
          `Module ${nodeModule
            .slice(0, i + 1)
            .join('.')} isn't currently supported.`,
          requestId,
        );
        return;
      }
    }
    console.log('[DAT] Zombie module:', zombie);
    const requestArray = requestId.split('*');
    const requestType = requestArray[0];
    // const timeInSeconds = requestArray[1];
    // const randomId = requestArray[2];
    var result;
    var callbacks = {};
    for (var i = 0; i < params.length; i++) {
      const param = params[i];
      if (param === '[FUNCTION]') {
        param.splice(i, 1, function fakeCallback() {
          callbacks[i] = Array.from(arguments);
        });
      }
    }
    if (requestType === '()') {
      // Constructor
      try {
        result = await promisify(zombie, params);
      } catch (promiseError) {
        bridgeRequestError(`${promiseError}`, requestId);
        return;
      }
    } else if (requestType.indexOf('[BUFFER]') !== -1) {
      // Get buffer
      const request = requestType.replace('[BUFFER]', '');
      result = zombie[request];
      // TODO - Send buffer accross bridge (safely?)
    } else if (zombie[requestType]) {
      // General Function
      try {
        result = await promisify(zombie[requestType], params);
      } catch (promiseError) {
        bridgeRequestError(`${promiseError}`, requestId);
        return;
      }
    } else {
      bridgeRequestError(
        `Module ${nodeModule
          .concat([requestType])
          .join('.')} isn't currently supported.`,
        requestId,
      );
      return;
    }
    console.log('[DAT] Request result:', result);
    if (result && (typeof result === 'object' || isClass(result))) {
      result.uniqueNodeId = requestId;
    }
    const results = {callbacks, result};
    zombified[requestId] = results;
    const zombieResult = zombify(results, {name: requestId, depth: 2});
    console.log('[DAT] Zombified results for ', requestId, ':', zombieResult);
    rn_bridge.channel.post(requestId, zombieResult);
  },
);
