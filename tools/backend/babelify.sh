#!/bin/bash

# Any copyright is dedicated to the Public Domain.
# http://creativecommons.org/publicdomain/zero/1.0/

set -eEu -o pipefail
shopt -s extdebug
IFS=$'\n\t'
trap 'onFailure $?' ERR

function onFailure() {
  echo "Unhandled script error $1 at ${BASH_SOURCE[0]}:${BASH_LINENO[0]}" >&2
  exit 1
}

cd nodejs-assets/nodejs-project;
mkdir babelified_modules;

npx babel node_modules --out-dir babelified_modules;
ditto ./babelified_modules ./node_modules;

rm -rf babelified_modules;

# cd lib
# npx regenerator generators.js > _generators.js
# gawk -i inplace '/Symbol\.iterator/{gsub("Symbol.iterator","\"@@asyncIterator\"");}1' _generators.js
# rm generators.js; mv _generators.js generators.js;

# cd ../../..;
cd ../..;
