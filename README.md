# Gateway Browser

![logo.png](repo-assets/256x256.png)

Gateway is an experimental peer-to-peer mobile Web browser for iOS and Android.

It gives you access to distributed protocols like the [hypercore protocol](https://hypercore-protocol.org/) while remaining compatible with the rest of the Web

Follow Gateway on [Twitter](https://twitter.com/GatewayBrowser) for dev updates!

## ⚠️ This project is currently in alpha ⚠️
Many features may be incomplete, unstable, or not optimized. It's functional, but not recommended for regular use yet. Please reference the [known issues](#known-issues) and [roadmap](#roadmap) to see where the app is currently at.

## Table of Contents
- [Installing](#installing)
  - [Binaries](#binaries)
  - [Building from source](#building-from-source)
    - [iOS](#ios)
    - [Android](#android)
- [Vulnerability Disclosure](#vulnerability-disclosure)
- [Contributing](#contributing)
  - [Comment Syntax](#comment-syntax)
  - [Known Issues](#known-issues)
- [Roadmap](#roadmap)
  - [Alpha](#alpha)
  - [Beta](#beta)

## Installing

### Binaries

**Will be released once the project is in pre-release beta.**

### Building from source

Here's how to build and run Gateway on your phone if you're cloning/downloading this repo:

##### Step 0

Install [React Native](https://reactnative.dev/docs/environment-setup)

For iOS: Install Xcode

For Android: Install Android Studio

#### iOS (Will need a Mac)

##### Step 1

Plug your phone into your computer

##### Step 2

Open [package.json](./package.json) and make sure `"iPhone"` is replaced with your device name

##### Step 3

Release Build (If you just want to try out the app, use this):

```
In terminal:
npm run ios
```

Debug Build:

```
In terminal:
npm run ios-dev
```

If you're having issues, try this:

```
In terminal:
npm run build-backend-ios-fix

With Xcode:
Open 'ios/Gateway.xcworkspace'
Go to 'Product -> Scheme -> Edit Scheme' to choose either 'Debug' or 'Release' build configuration
```

#### Android

##### Step 1

Plug your phone into your computer

##### Step 2

Release Build (If you just want to try out the app, use this):

```
In terminal:
npm run android
```

Debug Build:

```
In terminal:
npm run android-dev
```

If you're having issues, try this:

```
In terminal:
npm run build-backend-android-fix

// For Release
react-native run-android --variant=release

// For Debug
react-native run-android --variant=debug
```

## Vulnerability Disclosure

See [SECURITY.md](./SECURITY.md) for reporting security issues and vulnerabilities.

## Contributing

### Comment Syntax

> TODO H - High priority (Either currently being worked on or needs to be)

> TODO M - Medium priority (Will be priority after all high priority tasks are complete)

> TODO L - Low priority (Either a far away task or a small detail/option)

### Known Issues

#### Initial Hyperdrive Loading Inconsistency

When first trying to load a [hyperdrive](https://github.com/hypercore-protocol/hyperdrive), it may fail to load or take you to an empty directory at first. This is because of some unknown bug after upgrading to the new [hypercore-protocol](https://hypercore-protocol.org/).

#### Cellular Data

[Hyperswarm](https://github.com/hyperswarm/hyperswarm) currently doesn't explicitly support cellular networks, so you may have issues using hyper:// depending on your cellular provider. There's currently a [workaround](src/backend/lib/corestore-swarm.js) in place that adds [hyperswarm-web](https://github.com/RangerMauve/hyperswarm-web) to [corestore-swarm-networking](https://github.com/andrewosh/corestore-swarm-networking), but this only works if the desired [hyperdrive](https://github.com/hypercore-protocol/hyperdrive) or [hypercore](https://github.com/hypercore-protocol/hypercore) is also being replicated with hyperwswarm-web (which could be another phone running Gateway on WiFi).

There is an [open issue](https://github.com/hyperswarm/hyperswarm/issues/63) for mobile support on the hyperswarm repo.

#### WebView Race Condition

There's a race condition that's currently being investigated but may cause some P2P elements on web pages to either not appear or duplicate. This is more common if the connection is slower. If you encounter this issue, try refreshing the page and this will often load everything up.

#### Consecutive custom HTML elements

If one P2P site defines custom HTML elements that are defined in a following site, those elements may have trouble loading on the next site. This is due to a quirk with WebView that is being investigated.

## Roadmap

### Alpha

- [x] Web navigation
- [x] Hyper://
- [x] Files Explorer
- [x] QR Codes
  - [x] Scanning
  - [x] Generating
- [x] Custom Protocol Fetch API
  - [ ] Finish hyper:// methods
- [x] Custom Protocol URL Parsing
- [x] Beaker APIs
  - [x] [Hyperdrive](https://docs.beakerbrowser.com/apis/beaker.hyperdrive)
    - [x] Reading
    - [ ] Writing
  - [x] [Peersockets](https://docs.beakerbrowser.com/apis/beaker.peersockets)
  - [x] [Markdown](https://docs.beakerbrowser.com/apis/beaker.markdown)
  - [ ] [Capabilities](https://docs.beakerbrowser.com/apis/beaker.capabilities)
  - [ ] [Contacts](https://docs.beakerbrowser.com/apis/beaker.contacts) (Need identities first)
- [ ] Long-press web elements (i.e. saving images)
- [ ] Privacy
  - [ ] Disable Cookies
  - [ ] Block Trackers
  - [ ] Block Ads
  - [ ] Block Scripts (User-Selected)
  - [ ] Digital Fingerprint Protection
- [ ] Tabs
  - [ ] Tab Navigation
  - [ ] Background Tabs
  - [ ] Tab Grouping/Nesting
- [ ] P2P Push Notifications
- [ ] Bookmarks
- [ ] Full History
- [ ] Incognito Mode
- [ ] Protocol Importing
  - [ ] External API
  - [ ] Test with:
    - [ ] Kappa-Core
    - [ ] Cabal
    - [ ] SSB
- [ ] API Importing
  - [ ] External API

### Beta

- [ ] Appearance
  - [ ] Custom Settings
    - [ ] Default
    - [ ] Site-Specific
  - [ ] Chameleon Mode
- [ ] Mesh Network
- [ ] P2P Domain Names
- [ ] Identities
- [ ] Co-Hosting
  - [ ] In-App
  - [ ] Desktop Delegate (Beaker, Aggregore, etc.)
- [ ] Create/Edit/Fork Nodes
- [ ] Distributed Native Apps
- [ ] Maker
